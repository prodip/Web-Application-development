<?php

$x = 10;
echo 'Pre increments'.'<br>';
echo 'Result:'. ++$x."\n";  // output: 11
echo 'Result:'. $x;  // output: 11
echo '<br>';

echo 'Post increments'. '<br>';
$y = 10;
echo 'Result:'. $y++ .'<br>';  // output: 10
echo 'Result:'. $y;  // output: 11

echo '<br>';

$m = 10;
echo 'Pre decrements'.'<br>';
echo 'Result:'. --$m."\n";  // output: 9
echo 'Result:'. $m;  // output: 9
echo '<br>';

echo 'Post decrements'. '<br>';
$n = 10;
echo 'Result:'. $n-- .'<br>';  // output: 10
echo 'Result:'. $n;  // output: 9

