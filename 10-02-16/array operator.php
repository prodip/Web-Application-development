<?php

$x = array('a' => 'apple', 'b' => 'ball');
$y = array('a' => 'pear', 'b' => 'strawberry', 'c' => 'cherry');

$z = $x + $y;
var_dump($z);
echo '<pre>';

$m = array('apple', 'banana');
$n = array(1 => 'banana', 0 => 'apple');

var_dump($m == $n);
var_dump($m === $n);
var_dump($m != $n);
var_dump($m <> $n);
var_dump($m !== $n);

