<?php

$a = true and 5;
var_dump($a);

echo '<pre>';
$g = true && false;
$h = true and false;
var_dump($g, $h);

echo '<pre>';
$x = true or FALSE;
var_dump($x);

echo '<pre>';
$y = true || FALSE;
var_dump($y);

echo '<pre>';
//$z = 3;
$z = 5 != 8;
var_dump($z);

echo '<pre>';
//$z = 3;
$m = TRUE xor FALSE;
var_dump($m);