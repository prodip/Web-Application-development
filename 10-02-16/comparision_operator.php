<?php

$x = 5;
$y = 5;

var_dump($x == $y);
echo '<br>';

var_dump(0 == "a");
echo '<br>';

var_dump(0 === "a");
echo '<br>';

var_dump(5 != "a");
echo '<br>';

var_dump(5 <> 5);
echo '<br>';

var_dump(5 !== 8);
echo '<br>';

var_dump(5 > 8);
echo '<br>';

var_dump(5 < 8);
echo '<br>';

var_dump(5 >= 8);
echo '<br>';

var_dump(5 <= 8);
