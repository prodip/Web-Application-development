<?php
// Equal
$a = 20;
$b = 5;

$a = $b;
echo $a;    // output 5
echo '<br>';
// Addition
$x = 25;
$y = 5;

$x += $y;
echo $x;    // output 5
echo '<br>';
// Subtraction
$m = 20;
$n = 5;

$m -= $n;
echo $m;    // output 5
echo '<br>';
// Multiplication

$o = 20;
$p = 5;
$o *= $p;
echo $o;    // output 5
echo '<br>';
// Division
$c = 20;
$d = 5;

$c/= $d;
echo $c;    // output 5
echo '<br>';
// Modulus 
$t = 20;
$s = 5;

$t%= $s;
echo $t;    // output 5
echo '<br>';

