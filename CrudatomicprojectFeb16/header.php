<!DOCTYPE html>
<html>
    <head>
        <title>
            BITM Atomic Projects | Home Page
        </title>
        <link rel="stylesheet" type="text/css" href="../../../../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../../../css/custom.css">
    </head>
    <body>

        <div class="container">
            <div class="" style="background-color: #eee;">
                <div class="row">
                    <div class="col-md-3">
                        <img src="../../../../img/logo.png" height="80" style="margin: 40px 0 0 65px;"> 
                    </div>
                    <div class="col-md-9">
                        <img src="../../../../img/project_banner.jpg" width="100%" height="170">
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="../../../../index.php">Home</a></li>
                            <li><a href="../../../../views/Bitm/SEIP_113264/Book/index.php">Book</a></li>
                            <li><a href="../../../../views/Bitm/SEIP_113264/Mobile/index.php">Mobile</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Birthday/index.php">Birthday</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Gender/index.php">Gender</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Hobby/index.php">Hobby</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/ProfilePicture/index.php">Profile Picture</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Subscription/index.php">Subscription</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/City/index.php">City</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Summary/index.php">Summary</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Slider/index.php">Sliders</a></li> 
                            <li><a href="../../../../views/Bitm/SEIP_113264/Term_Condition/index.php">Term and Condition</a></li> 

                        </ul>
                       
                    </div>
                </div>
            </nav>
