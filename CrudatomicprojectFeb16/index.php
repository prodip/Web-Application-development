<?php
include_once 'vendor/autoload.php';
 
 use App\Bitm\SEIP_113264\ProfilePicture\profilePicture;
 use App\Bitm\SEIP_113264\Slider\slider;
 
 $sliderobj = new slider();
 $Allsliders = $sliderobj->sliders();
 
$profilepicobj = new profilePicture();
$Alldata = $profilepicobj->Profile_Picture();
//print_r($Allsliders);
//exit();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            BITM Atomic Projects | Home Page
        </title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>

        <div class="container">
            <div class="" style="background-color: #eee;">
                <div class="row">
                    <div class="col-md-3">
                        <img src="img/logo.png" height="80" style="margin: 40px 0 0 65px;"> 
                    </div>
                    <div class="col-md-9">
                        <img src="img/project_banner.jpg" width="100%" height="170">
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="views/Bitm/SEIP_113264/Book/index.php">Book</a></li>
                            <li><a href="views/Bitm/SEIP_113264/Mobile/index.php">Mobile</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Birthday/index.php">Birthday</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Gender/index.php">Gender</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Hobby/index.php">Hobby</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/ProfilePicture/index.php">Profile Picture</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Subscription/index.php">Subcription</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/City/index.php">City</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Summary/index.php">Summary</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Slider/index.php">Sliders</a></li> 
                            <li><a href="views/Bitm/SEIP_113264/Term_Condition/index.php">Term and Condition</a></li> 
                        </ul>
                       
                    </div>
                </div>
            </nav>

            <div class="row">
                <div class="col-md-8">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" style="height: 350px; overflow: hidden;">
                            <?php 
                                foreach($Allsliders as $v_slider){
                            ?>
                            <div class="item active">
                                <img src="<?php echo 'img/sliders/'.$v_slider['slider_image'];?>" width="100%" height="300">
                                <div class="carousel-caption">
                                    <h3>Chania</h3>
                                    <p><?php echo $v_slider['caption'];?></p>
                                </div>
                            </div>
                                <?php }?>
                            <div class="item">
                                <img src="img/sliders/14599659111017186_261013754039523_1524436942_n.jpg" width="100%" alt="Chania">
                                <div class="carousel-caption">
                                    <h3>Chania</h3>
                                    <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div class="panel" style="margin-top: 25px;">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1415261415.jpg" alt="...">
                                <div class="caption">
                                    <h3>Projects of BITM</h3>
                                    <p>To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology & Management (BITM) </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1415261370.jpg" alt="...">
                                <div class="caption">
                                    <h3>First Track Project</h3>
                                    <p>To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology & Management (BITM) </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1416294158.jpg" alt="...">
                                <div class="caption">
                                    <h3>Outsourcing Award</h3>
                                    <p>To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology & Management (BITM) </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>


                </div>




                <div class="col-md-4">
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <div class="" style="margin: 80px 0 0 50px;">
                        <img src="<?php echo 'img/profile_pics/'.$Alldata['profile_pic'];?>" class="img-thumbnail" width="220" height="330">
                        <h3 ><?php echo $Alldata['name'];?></h3>
                    </div>
                </div>
            </div>






        </div>

        
        
        <footer>
            <div class="container" style=" height: 50px; color: #fff; padding-top: 15px; background-color:  #1b6d85; text-align: center;">
                <p>Xtreme Template @DipCoder</p>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>






