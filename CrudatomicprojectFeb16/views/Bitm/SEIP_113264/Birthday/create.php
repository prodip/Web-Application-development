<?php
include_once '../../../../header.php';
session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Birthday | Create Page
        </title>
    </head>
    <body>
        <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> 
                    


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="store.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Add Birthday Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name">
                <span>
                    <?php 
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter Your Birthday:</label>
                <input type="date" name="birthday">
                <span>
                    <?php 
                        if(isset($_SESSION['birthdayerr'])){
                            echo $_SESSION['birthdayerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter  Birthday Place:</label>
                <input type="text" name="b_place">
                <span>
                    <?php 
                        if(isset($_SESSION['b_placerr'])){
                            echo $_SESSION['b_placerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        </div>
        
    </body>
</html>
