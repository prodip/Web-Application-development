<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Birthday\birthdayClass;

$birthdayobj = new birthdayClass();
$id = $_GET['id'];
$singleData = $birthdayobj->show($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Birthday | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="update.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Add Birthday Information Here</legend>
                <label>Edit Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $singleData['name'];?>">
                <input type="hidden" name="id" value="<?php echo $singleData['id'];?>">
                <br>
                <label>Edit Your Birthday:</label>
                <input type="date" name="birthday" value="<?php echo $singleData['birthday'];?>">
                <br>
                <label>Edit  Birthday Place:</label>
                <input type="text" name="b_place" value="<?php echo $singleData['b_place'];?>">
                <br>
                
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        </div>
        
    </body>
</html>

