<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Birthday\birthdayClass;

$birthdayobj = new birthdayClass();
$id = $_GET['id'];
$singleData = $birthdayobj->show($id);

 $data= $singleData['birthday'];
 
 $f_data = date('d-m-Y',  strtotime($data));
     //    echo '<pre>';
    //     print_r($f_data);
    //     exit();
 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Details Page
        </title>
    </head>
    <body>
         <h1 align="center">View Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>

                </div>
            </div>
        </div>

        <div class="content" align="center">
       
        <table border="1" style="font-size: 22px;">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Birthday</th>
              <th>Birthday Place</th>
              <th>Action</th>
            </thead>
            <tbody>
               <tr>
                    <td><?php echo $singleData['id'];?></td>
                    <td><?php echo $singleData['name'];?></td>
                    <td><?php echo $f_data;?></td>
                    <td><?php echo $singleData['b_place'];?></td>
                    <td>
                       <a href="show.php?id=<?php echo $singleData["id"]; ?>">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                View
                            </button>
                        </a> | 
                        <a href="edit.php?id=<?php echo $singleData["id"]; ?>">
                            <button type="button" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                Edit
                            </button>
                        </a> | 
                        <a href="trash.php?id=<?php echo $singleData["id"]; ?>">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                Delete
                            </button>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        </div>
        
    </body>
</html>


