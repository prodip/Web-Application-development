<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Gender\gender;

$genderobj = new gender();

$id = $_GET['id'];
$singleData = $genderobj->view($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Gender | View Page
        </title>
    </head>
    <body>
         <h1 align="center">View Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>

                </div>
            </div>
        </div>

        <div class="content" align="center">
       
        <table border="1" style="font-size: 22px;">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Term and Condition</th>
              <th>Action</th>
            </thead>
            <tbody>
               
                <tr>
                    <td><?php echo $singleData['id'];?></td>
                    <td><?php echo $singleData['name'];?></td>
                    <td><?php echo $singleData['gender'];?></td>
                    <td> <input type="checkbox" name="term"  <?php if($singleData['term_and_condition'] ==1){ echo 'checked';};?>></td>
                    <td>
                       <a href="show.php?id=<?php echo $singleData["id"]; ?>">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                View
                            </button>
                        </a> | 
                        <a href="edit.php?id=<?php echo $singleData["id"]; ?>">
                            <button type="button" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                Edit
                            </button>
                        </a> | 
                        <a href="trash.php?id=<?php echo $singleData["id"]; ?>">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                Delete
                            </button>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        </div>
        
    </body>
</html>
