<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\City\city;
use App\Bitm\SEIP_113264\Utility\utility;

$cityobj = new city();
$id = $_GET['id'];
$edit_info = $cityobj->view($id);
?>
<html>
    <head>
        <title>
            City | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">City Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">

            <form action="update.php" method="POST">
                <fieldset>
                    <legend>Edit Your City INFO</legend>
                    <table>
                        <tr>
                            <td><label for="name">Your Name</label></td>
                            <td>
                                <input type="text" name="name" id="name" value="<?php echo $edit_info['name']; ?>">
                                <input type="hidden" name="id" value="<?php echo $edit_info['id']; ?>">
                            </td>
                            <td>
                                <?php

                                if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                                    echo utility::message(NULL);
                                }
                               
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><label>City</label></td>
                            <td>
                                <select name="city">
                                    <option value="Dhaka" <?php
                                    if ($edit_info['city'] == 'Dhaka') {
                                        echo 'selected';
                                    }
                                    ?>>Dhaka</option>
                                    <option value="Chittagong" <?php
                                    if ($edit_info['city'] == 'Chittagong') {
                                        echo 'selected';
                                    }
                                    ?>>Chittagong</option>
                                    <option value="Rajshahi" <?php
                                            if ($edit_info['city'] == 'Rajshahi') {
                                                echo 'selected';
                                            }
                                            ?>>Rajshahi</option>
                                    <option value="Barishal" <?php
                                    if ($edit_info['city'] == 'Barishal') {
                                        echo 'selected';
                                    }
                                    ?>>Barishal</option>
                                    <option value="Comilla" <?php
                                    if ($edit_info['city'] == 'Comilla') {
                                        echo 'selected';
                                    }
                                    ?>>Comilla</option>
                                    <option value="Sylhet" <?php
                                    if ($edit_info['city'] == 'Sylhet') {
                                        echo 'selected';
                                    }
                                    ?>>Sylhet</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" class="btn btn-success pull-right" style="margin-right:140px; " value="Update">
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
    </body>
</html>
