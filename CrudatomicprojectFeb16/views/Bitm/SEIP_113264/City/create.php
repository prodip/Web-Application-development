<?php
include_once '../../../../header.php';
session_start();
//var_dump($_SESSION);
?>
<html>
    <head>
        <title>
            City | Create Page
        </title>
    </head>
    <body>
         <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add Your City</legend>
                <table>
                    <tr>
                        <td><label for="name">Your Name</label></td>
                        <td><input type="text" name="name" id="name"></td>
                        <td>
                            <?php 
                                if(isset($_SESSION['namerr'])){
                                echo $_SESSION['namerr'];
                                unset($_SESSION['namerr']);
                                }
                            ?>
                        </td>
                </tr>
                <tr>
                    <td><label>Select City</label></td>
                    <td>
                        <select name="city">
                            <option value="">Select City</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Chittagong">Chittagong</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Barishal">Barishal</option>
                            <option value="Comilla">Comilla</option>
                            <option value="Sylhet">Sylhet</option>
                        </select>
                    </td>
                    <td>
                            <?php 
                                if(isset($_SESSION['cityerr'])){
                                echo $_SESSION['cityerr'];
                                unset($_SESSION['cityerr']);
                                }
                            ?>
                    </td>
                </tr>
                
                <tr>
                <td><input type="submit" value="Save">
                    <input type="reset" value="Reset"></td>
                </tr>
                </table>
            </fieldset>
        </form>
        </div>
    </body>
</html>
