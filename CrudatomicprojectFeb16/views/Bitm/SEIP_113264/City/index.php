

<?php
include_once '../../../../vendor/autoload.php';
include_once '../../../../header.php';

use App\Bitm\SEIP_113264\City\city;
use App\Bitm\SEIP_113264\Utility\utility;

$cityobj = new city();
$Alldata = $cityobj->index();
//var_dump($Alldata);
?>
<html>
    <head>
        <title>
            City | Home Page
        </title>
    </head>
    <body>

        <h1 align="center">City Atomic Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="trashed.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                            Deleted Items
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>
                    <select class="btn" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">

            <?php
//        print_r($_SESSION);
//        exit();
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                echo $_SESSION['message'];
                echo utility::message();
            }
            if (isset($_SESSION['mgs'])) {
                echo $_SESSION['mgs'];
                unset($_SESSION['mgs']);
            }
            ?>
            <table border='1'style="font-size: 22px;">
                <th>SI</th>
                <th>ID</th>
                <th>Name</th>
                <th>City</th>
                <th>Action</th>
                <?php
                $serial = 0;
                foreach ($Alldata as $v_city) {
                    $serial++;
//                    var_dump($v_book);
//                    exit();
                    ?>
                    <tr>
                        <td><?php echo $serial; ?></td>
                        <td><?php echo $v_city["id"]; ?></td>
                        <td><?php echo $v_city["name"]; ?></td>
                        <td><?php echo $v_city["city"]; ?></td>
                        <td>
                            <a href="show.php?id=<?php echo $v_city["id"]; ?>">
                                <button type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                    View
                                </button>
                            </a> | 
                            <a href="edit.php?id=<?php echo $v_city["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Edit
                                </button>
                            </a> | 
                            <a href="trash.php?id=<?php echo $v_city["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Delete
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </table>
          

        </div>


    </body>
</html>
<?php include_once '../../../../footer.php'; ?>


