<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Slider\slider;

$sliderobj = new slider();

//$image = $_FILES['profile_pic'];

$image_name = $_FILES['slider_image']['name'];
$image_type = $_FILES['slider_image']['type'];
$image_location = $_FILES['slider_image']['tmp_name'];
$image_size = $_FILES['slider_image']['size'];

$image = time().$image_name;

$m = explode('.', $image_name);
$exp = array_pop($m);
$image_type_support = array('jpg', 'jpeg', 'png');

if(in_array($exp, $image_type_support) === false){
    echo 'Invalid Format!';
}else{
    $_POST['slider_image'] = $image;
    move_uploaded_file($image_location, '../../../../img/sliders/'.$image);
}

//echo '<pre>';
//print_r($image);
//exit();


$sliderobj->prepare_data($_POST);
$sliderobj->insert();

