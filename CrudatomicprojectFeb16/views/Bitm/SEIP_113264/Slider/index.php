<?php
 include_once '../../../../header.php';
 include_once '../../../../vendor/autoload.php';
 
 use App\Bitm\SEIP_113264\Slider\slider;
 use App\Bitm\SEIP_113264\Utility\utility;
 
$sliderobj = new slider();
$Alldata = $sliderobj->index();
//print_r($Alldata);
//exit();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Slider | Home Page
        </title>
    </head>
    <body>
        <h1 align="center">Slider Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="trashed.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                            Deleted Items
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>
                    <select class="btn" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">

            <?php
//        print_r($_SESSION);
//        exit();
            if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
             //   echo $_SESSION['message'];
                echo utility::message(NULL);
            }
            
            ?>
            <table border='1'style="font-size: 22px;">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Slider Image</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($Alldata as $v_info){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_info['id'];?></td>
                    <td><img src="<?php echo '../../../../img/sliders/'.$v_info['slider_image'];?>" width="500" height="230"></td>
                    <td>
                         <a href="show.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                    View
                                </button>
                            </a> | 
                            <a href="edit.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Edit
                                </button>
                            </a> | 
                            <?php
                                if($v_info["status"] == 0){
                            ?>
                            <a href="statusAC.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Active
                                </button>
                            </a> | 
                                <?php }else{?>
                            <a href="statusDC.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Deactive
                                </button>
                            </a> | 
                                <?php }?>
                            <a href="trash.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Delete
                                </button>
                            </a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        
        
        </div>
    </body>
</html>
<?php include_once '../../../../footer.php';?>