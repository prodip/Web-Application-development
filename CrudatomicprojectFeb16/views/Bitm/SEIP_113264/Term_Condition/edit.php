<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Term_Condition\term;

$termobj = new term();

$id = $_GET['id'];
$singleData = $termobj->view($id);
//print_r($singleData);
//exit();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Term And Condition | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="update.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Add Term And Condition Information Here</legend>
                <input type="hidden" name="id" value="<?php echo $singleData['id'];?>">
                <input type="checkbox" name="term" value="1" <?php if($singleData['term_and_condition'] ==1){ echo 'checked';};?>> You should check this term and condition Box.
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        </div>
        
    </body>
</html>
