<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Term_Condition\term;

$termobj = new term();
$termobj->prepare($_POST);
$termobj->store();
