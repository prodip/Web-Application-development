<?php
include_once '../../../../header.php';
session_start();
//var_dump($_SESSION);
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Term and Condition | Create Page
        </title>
    </head>
    <body>
        <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> 
                    


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="store.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Add Term And Condition Information Here</legend>
                <input type="checkbox" name="term_and_condition" value="1"> You should Check this term and condition
                <span>
                    <?php
                       if(isset($_SESSION['termerr'])){
                      echo $_SESSION['termerr'];
                      unset($_SESSION['termerr']);
                     }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        </div>
        
    </body>
</html>