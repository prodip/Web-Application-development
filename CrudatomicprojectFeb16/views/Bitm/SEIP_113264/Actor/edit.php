<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Actor\actor;

$actorobj = new actor();

$id = $_GET['id'];
$single_view = $actorobj->view($id);

$exp = explode(',',$single_view['actor']);
$single_view['actor'] = $exp;
//print_r($single_view['name']);
//die();


?>
<html>
    <head>
        <title>
            Actor | Edit Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="update.php" method="POST" >
            <fieldset>
                <legend>Add Favorite Actors  Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $single_view['name'];?>">
                <input type="hidden" name="id" value="<?php echo $single_view['id'];?>">
                <br>
                <label>Select Actors:</label>
                 <input type="checkbox" name="actor[]" value="Imran Khan" <?php if(in_array('Imran Khan',$single_view['actor'])){echo 'checked';};?>>Imran Khan
                <input type="checkbox" name="actor[]" value="Khorshed Alam" <?php if(in_array('Khorshed Alam',$single_view['actor'])){echo 'checked';};?>>Khorshed Alam
                <input type="checkbox" name="actor[]" value="Tofayal Khan" <?php if(in_array('Tofayal Khan',$single_view['actor'])){echo 'checked';};?>>Tofayal Khan
                <input type="checkbox" name="actor[]" value="Sharukh Khan" <?php if(in_array('Sharukh Khan',$single_view['actor'])){echo 'checked';};?>>Sharukh Khan
                <input type="checkbox" name="actor[]" value="Salman Khan" <?php if(in_array('Salman Khan',$single_view['actor'])){echo 'checked';};?>>Salman Khan
                <input type="checkbox" name="actor[]" value="Mahfuz Khan" <?php if(in_array('Mahfuz Khan',$single_view['actor'])){echo 'checked';};?>>Mahfuz Khan
                <input type="checkbox" name="actor[]" value="Nirob Khan" <?php if(in_array('Nirob Khan',$single_view['actor'])){echo 'checked';};?>>Nirob Khan
                <input type="checkbox" name="actor[]" value="Amir Khan" <?php if(in_array('Amir Khan',$single_view['actor'])){echo 'checked';};?>>Amir Khan
                <input type="checkbox" name="actor[]" value="Ajoy Khan" <?php if(in_array('Ajoy Khan',$single_view['actor'])){echo 'checked';};?>>Ajoy Khan
                <input type="checkbox" name="actor[]" value="Mirron Khan" <?php if(in_array('Mirron Khan',$single_view['actor'])){echo 'checked';};?>>Mirron Khan
               
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>