<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Actor\actor;

$actorobj = new actor();

$id = $_GET['id'];
$single_view = $actorobj->view($id);


?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Actor | View Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="index.php">Home</a> 
      
        <table border="1">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Actors</th>
              <th>Action</th>
            </thead>
            <tbody>
               
                <tr>
                    <td><?php echo $single_view['id'];?></td>
                    <td><?php echo $single_view['name'];?></td>
                    <td><?php echo $single_view['actor'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $single_view['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $single_view['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $single_view['id'];?>">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        
        
    </body>
</html>