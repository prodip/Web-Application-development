<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Mobile\mobileClass;

$mobileobj = new mobileClass();
$id = $_GET['id'];
$edit_info = $mobileobj->show($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Edit Page
        </title>
    </head>
    <body>
        <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="update.php" method="POST">
            <fieldset style="text-align: left;">
                <legend>Edit Your Favourite Mobile Model</legend>
                <label>Enter Mobile Name:</label>
                <input type="text" name="title" value="<?php echo $edit_info['title'];?>">
                <input type="hidden" name="id" value="<?php echo $edit_info['id'];?>">
                <br>
                
                <label>Enter Mobile Model:</label>
                <input type="text" name="model" value="<?php echo $edit_info['model'];?>">
                <br>
                
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        </div>
    </body>
</html>