<?php
 include_once '../../../../header.php';
 include_once '../../../../vendor/autoload.php';
 
use App\Bitm\SEIP_113264\Hobby\hobby;
use App\Bitm\SEIP_113264\Utility\utility;

$hobbyobj = new hobby();

$all_info = $hobbyobj->trashed();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Hobby | Trashed Page
        </title>
    </head>
    <body>
       <h1 align="center">Gender Atomic Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="index.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>
                    <select class="btn" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">
        <?php
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
              //  echo $_SESSION['message'];
                echo utility::message(NULL);
            }
        
        ?>
        <table border="1" style="font-size: 22px;">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Hobbies</th>
              <th>Action</th>
            </thead>
            <tbody>
               <?php
                $s = 0;
                foreach($all_info as $v_info){
                 $s++;   
               ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_info['id'];?></td>
                    <td><?php echo $v_info['name'];?></td>
                    <td><?php echo $v_info['hobby'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                    View
                                </button>
                            </a> | 
                            <a href="edit.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Edit
                                </button>
                            </a> | 
                            <a href="restore.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Restore
                                </button>
                            </a> |
                            <a href="delete.php?id=<?php echo $v_info["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Delete
                                </button>
                            </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        
        </div>
        
    </body>
</html>



