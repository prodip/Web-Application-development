<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\ProfilePicture\profilePicture;
 
$profilepicobj = new profilePicture();
$id = $_GET['id'];
$single_view = $profilepicobj->view($id);
//echo "<pre>";
//print_r($single_view);
//exit();
 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Profile Picture | Details Page
        </title>
    </head>
    <body>
        <h1 align="center">View Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Homes
                        </button>
                    </a> |
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    
                    <a href="#">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Print Document
                        </button>
                    </a>

                </div>
            </div>
        </div>

        <div class="content" align="center">

          
            <table border='1'style="font-size: 22px; margin-bottom: 50px;">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Profile Pic</th>
              <th>Action</th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $single_view['id'];?></td>
                    <td><?php echo $single_view['name'];?></td>
                    <td><img src="<?php echo '../../../../img/profile_pics/'.$single_view['profile_pic'];?>" width="220" height="230"></td>
                    <td>
                       <a href="show.php?id=<?php echo $single_view["id"]; ?>">
                            <button type="button" class="btn btn-primary">
                                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                View
                            </button>
                        </a> | 
                        <a href="edit.php?id=<?php echo $single_view["id"]; ?>">
                            <button type="button" class="btn btn-success">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                Edit
                            </button>
                        </a> | 
                        <a href="trash.php?id=<?php echo $single_view["id"]; ?>">
                            <button type="button" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                Delete
                            </button>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        
        </div> 
    </body>
</html>

