<?php
include_once '../../../../header.php';
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\ProfilePicture\profilePicture;
 
$profilepicobj = new profilePicture();
$id = $_GET['id'];
$edit_info = $profilepicobj->view($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Profile Picture | Edit Page
        </title>
    </head>
    <body>
         <h1 align="center">Edit Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="update.php" method="POST" enctype="multipart/form-data">
                <fieldset  style="text-align: left;">
                <legend>Add Your Profile Picture info</legend>
                <label>Enter Your Name:</label>
                <input type="text" name="name" value="<?php echo $edit_info['name'];?>">
                <input type="hidden" name="id" value="<?php echo $edit_info['id'];?>">
                <br>
                
                <label>Choose A Image:</label>
                <input type="file" name="profile_pic">
                <img src="<?php echo '../../../../img/profile_pics/'.$edit_info['profile_pic'];?>" width="220" height="230">
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        </div>
    </body>
</html>