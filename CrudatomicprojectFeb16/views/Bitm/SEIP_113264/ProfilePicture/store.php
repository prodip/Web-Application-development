<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\ProfilePicture\profilePicture;

$profilepicobj = new profilePicture();

//$image = $_FILES['profile_pic'];

$image_name = $_FILES['profile_pic']['name'];
$image_type = $_FILES['profile_pic']['type'];
$image_location = $_FILES['profile_pic']['tmp_name'];
$image_size = $_FILES['profile_pic']['size'];
//$path = '../../../../img/profile_pics/';

$image = time().$image_name;

$m = explode('.', $image_name);
$exp = array_pop($m);
$image_type_support = array('jpg', 'jpeg', 'png');

if(in_array($exp, $image_type_support) === false){
    echo 'Invalid Format!';
}else{
    $_POST['profile_pic'] = $image;
    move_uploaded_file($image_location, '../../../../img/profile_pics/'.$image);
}

//echo '<pre>';
//print_r($image);
//exit();


$profilepicobj->prepare_data($_POST);
$profilepicobj->insert();

