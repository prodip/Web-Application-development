<?php
include_once '../../../../vendor/autoload.php';
include_once '../../../../vendor/phpmailer/phpmailer/class.phpmailer.php';
include_once '../../../../vendor/phpmailer/phpmailer/class.smtp.php';

use App\Bitm\SEIP_113264\Book\BookClass_File;

$bookobj = new BookClass_File();
$allData = $bookobj->index();

$trs = "";

$s = 0;
foreach ($allData as $data):
    $s++;

    $trs .= "<tr>";
    $trs .= "<td>" . $s . "</td>";
    $trs .= "<td>" . $data['id'] . "</td>";
    $trs .= "<td>" . $data['title'] . "</td>";
    $trs .= "<td>" . $data['author_name'] . "</td>";
    $trs .= "<td><img src=../../../../img/Book_cover_pics/" . $data['book_image'] . " width='200' height='200'></td>";
    $trs .= "</tr>";

endforeach;
//echo '<pre>';
//print_r($trs);
//exit();
$html = <<<EOD
 <html>
    <head>
        <title>
            Book | PDF Page
        </title>
    </head>
    <body>

        <h1 align="center" style="color:red;">Book Atomic Project</h1><hr>
       

        <div class="content" >
            <table border='1' align="center">
                <thead>
                    <tr style="background-color:green;">
                            <th>SI</th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Author Name</th>
                            <th>Book Cover Image</th>
                    </tr>
                </thead>
                <tbody>
                
                        $trs;
                </tbody>
            </table>
          
    </body>
</html>       
EOD;


$mail = new PHPMailer();


//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->SMTPDebug = 2;
$mail->Debugoutput = 'html';
$mail->Host = 'smtp.gmail.com';                         // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'prodip5080@gmail.com';                 // SMTP username
$mail->Password = '';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('prodip5080@gmail.com', 'Prodip Roy');
$mail->addAddress('prodip3080@gmail.com', 'Joe User');     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('info@example.com', 'Information');
$mail->addCC('cc@example.com');
$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
//$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Email Test';
$mail->Body    = $html;
$mail->AltBody = ' Test This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}

