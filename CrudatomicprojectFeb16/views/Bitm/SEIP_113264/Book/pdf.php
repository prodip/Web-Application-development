<style>
    table th {
        background-color: green;
    }
</style>

<?php

include_once '../../../../vendor/autoload.php';
include_once '../../../../vendor/mpdf/mpdf/mpdf.php';

use App\Bitm\SEIP_113264\Book\BookClass_File;

$bookobj = new BookClass_File();
$allData = $bookobj->index();
//echo '<pre>';
//print_r($allData);
//exit();
$trs = "";

$s = 0;
foreach ($allData as $data):
    $s++;

    $trs .= "<tr>";
    $trs .= "<td>" . $s . "</td>";
    $trs .= "<td>" . $data['id'] . "</td>";
    $trs .= "<td>" . $data['title'] . "</td>";
    $trs .= "<td>" . $data['author_name'] . "</td>";
    $trs .= "<td><img src=../../../../img/Book_cover_pics/" . $data['book_image'] . " width='200' height='200'></td>";
    $trs .= "</tr>";

endforeach;
//echo '<pre>';
//print_r($trs);
//exit();
$html = <<<EOD
 <html>
    <head>
        <title>
            Book | PDF Page
        </title>
    </head>
    <body>

        <h1 align="center" style="color:red;">Book Atomic Project</h1><hr>
       

        <div class="content" >
            <table border='1' align="center">
                <thead>
                    <tr style="background-color:green;">
                            <th>SI</th>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Author Name</th>
                            <th>Book Cover Image</th>
                    </tr>
                </thead>
                <tbody>
                
                        $trs;
                </tbody>
            </table>
          
    </body>
</html>       
EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;


//$mpdf = new mPDF();
//$mpdf = writeHTML($html);
//$mpdf = output();
//exit();

?>
