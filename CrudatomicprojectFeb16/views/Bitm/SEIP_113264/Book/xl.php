<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 4/10/2016
 * Time: 4:15 PM
 */
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_error', TRUE);
ini_set('display_startup_error', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a aWeb Browser');


include_once '../../../../vendor/autoload.php';
include_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';

use App\Bitm\SEIP_113264\Book\BookClass_File;

$bookobj = new BookClass_File();
$allData = $bookobj->show();

//echo '<pre>';
//print_r($allData);
//exit();


$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCategory("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, Generated using PHP classes.")
    ->setKeywords("Office 2007 openxml php")
    ->setCategory("Test result file");


$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'SI')
        ->setCellValue('B1', 'ID')
        ->setCellValue('C1', 'Book Title');

$counter = 2;
$serial = 0;

foreach($allData as $data){
    $serial++;

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter, $serial)
        ->setCellValue('B'.$counter, $data['id'])
        ->setCellValue('C'.$counter, $data['title']);

    $counter++;


}

$objPHPExcel->getActiveSheet()->setTitle('Book List');
//Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: atteachment;filename="01simple.xls"');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header ('Last-Modidied: '.gmdate ('D, d M Y H:i:s').'GMT');
header ('Cache-Control: cache, must-revalidate');
header ('Pragma: public');


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit();