<?php
include_once '../../../../header.php';
session_start();
//var_dump($_SESSION);
?>
<html>
    <head>
        <title>
            Book | Create Page
        </title>
    </head>
    <body>
         <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Books List
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add Your Favourite Book</legend>
                <table>
                    <tr>
                        <td><label>Add New Book</label></td>
                        <td><input type="text" name="title" id="title"></td>
                        <td>
                            <?php 
                                if(isset($_SESSION['titlerr'])){
                                echo $_SESSION['titlerr'];
                                unset($_SESSION['titlerr']);
                                }
                            ?>
                        </td>
                </tr>
                <tr>
                    <td><label>Author Name</label></td>
                    <td><input type="text" name="author_name"></td>
                    <td>
                            <?php 
                                if(isset($_SESSION['autherr'])){
                                echo $_SESSION['autherr'];
                                unset($_SESSION['autherr']);
                                }
                            ?>
                    </td>
                </tr>
                <tr>
                    <td><label>Select a Image</label></td>
                    <td><input type="file" name="book_image"></td>
                    <td>
                            <?php 
                                if(isset($_SESSION['imagerr'])){
                                echo $_SESSION['imagerr'];
                                unset($_SESSION['imagerr']);
                                }
                            ?>
                    </td>
                </tr>
                <tr>
                <td><input type="submit" value="Save">
                    <input type="reset" value="Reset"></td>
                </tr>
                </table>
            </fieldset>
        </form>
        </div>
    </body>
</html>
