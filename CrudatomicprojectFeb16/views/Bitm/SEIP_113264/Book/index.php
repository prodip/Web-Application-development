<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<?php
include_once '../../../../vendor/autoload.php';
include_once '../../../../header.php';


use App\Bitm\SEIP_113264\Book\BookClass_File;
use App\Bitm\SEIP_113264\Utility\utility;

$page1 = 0;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
    if ($page == '' || $page == '1') {
        $page1 = 0;
    } else {
        $page1 = ($page * 5) - 5;
    }
}


function add($num) {
    return $num;
}

if (isset($num)) {
    $r = $num;
} else {
    $r = 5;
}

$bookobj = new BookClass_File();
$Alldata = $bookobj->index($page1, $r);

//var_dump($Alldata);
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#show_num').change(function () {
            var num = $(this).val();
            document.write('<?php add("'+num+'"); ?>');

            add();
        });

        $('#mgs').hide(5000);

    });


</script>

<html>
    <head>
        <title>
            Book | Home Page
        </title>
    </head>
    <body>

        <h1 align="center">Book Atomic Project</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" style="font-size: 22px; margin-left: 13%">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New Book
                        </button>
                    </a> |
                    <a href="trashed.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                            Deleted Books
                        </button>
                    </a> |
                    <a href="pdf.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As PDF
                        </button>
                    </a> |
                    <a href="xl.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
                            Download As XL
                        </button>
                    </a> |
                    <a href="phpmailer.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-expand" aria-hidden="true"></span>
                            Send Email
                        </button>
                    </a>
                    <select class="btn" id="show_num" style="font-size: 16px; border: 1px solid #dcdcdc;">
                        <option value=''>Show</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
                </div>
            </div>
        </div>




        <div class="content" align="center">
            <div class="row" id="mgs" style="font-size: 20px; color: green; font-weight: bold;">
                <?php
                if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
                    echo utility::message(NULL);
                }
                ?>
            </div>
            <table border='1'style="font-size: 22px;">
                <th>SI</th>
                <th>ID</th>
                <th>Title</th>
                <th>Author Name</th>
                <th>Book Cover Image</th>
                <th>Action</th>
                <?php
                $serial = 0;
                foreach ($Alldata as $v_book) {
                    $serial++;
//                    var_dump($v_book);
//                    exit();
                    ?>
                    <tr>
                        <td><?php echo $serial; ?></td>
                        <td><?php echo $v_book["id"]; ?></td>
                        <td><?php echo $v_book["title"]; ?></td>
                        <td><?php echo $v_book["author_name"]; ?></td>
                        <td><img src="<?php echo '../../../../img/Book_cover_pics/'.$v_book['book_image'];?>" width="220" height="230"></td>
                        <td>
                            <a href="show.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                    View
                                </button>
                            </a> | 
                            <a href="edit.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-success">
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    Edit
                                </button>
                            </a> | 
                            <a href="trash.php?id=<?php echo $v_book["id"]; ?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    Delete
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php
            $cout = $bookobj->pagination();

            $a = $cout / 5;
            $a = ceil($a);

            for ($b = 1; $b <= $a; $b++) {
                ?>
                <a href="index.php?page=<?php echo $b; ?>" class="btn btn-primary" style="margin-top: 50px;"><?php echo $b . ' '; ?></a>


            <?php } ?>

        </div>


    </body>
</html>
<?php include_once '../../../../footer.php'; ?>


