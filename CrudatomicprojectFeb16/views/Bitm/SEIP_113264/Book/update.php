<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Book\BookClass_File;

$bookobj = new  BookClass_File();
$id = $_POST['id'];
$oldinfo = $bookobj->view($id);

$oldImage = $oldinfo['book_image'];
//$im = $_FILES['book_image'];
//print_r($im);
//exit();

$image_name = $_FILES['book_image']['name'];
$image_type = $_FILES['book_image']['type'];
$image_location = $_FILES['book_image']['tmp_name'];
$image_size = $_FILES['book_image']['size'];

$image = time().$image_name;

$m = explode('.', $image_name);
$exp = array_pop($m);
$image_type_support = array('jpg', 'jpeg', 'png');

if(in_array($exp, $image_type_support) === false){
    echo 'Invalid Format!';
}else{
    $_POST['book_image'] = $image;
    move_uploaded_file($image_location, '../../../../img/Book_cover_pics/'.$image);
    
    unlink('../../../../img/Book_cover_pics/'. $oldImage);
}




$bookobj->prepare_data($_POST);
$bookobj->update();

