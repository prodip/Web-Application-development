<?php

namespace App\Bitm\SEIP_113264\Term_Condition;

session_start();

use App\Bitm\SEIP_113264\Utility\utility;

class term {

    public $id = '';
    public $term_and_condition = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to Connect with Server!');
        mysql_select_db('db_crudprojectfeb16', $conn) or die('Unable to Select Database');
    }

    public function prepare($data = '') {
        //    echo '<pre>';
        //   print_r($data);
        //   exit();
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        $this->term_and_condition = $data['term_and_condition'];
    }

    public function store() {
        //   print_r( $this->term);
        //   exit();


        if (!empty($this->term_and_condition)) {
            $query = "INSERT INTO `db_crudprojectfeb16`.`term_and_condition` (`term_and_condition`) VALUES ('" . $this->term_and_condition . "');";
            //  print_r( $query);
            //  exit();
            mysql_query($query);

            utility::message("Saved Successfully!");
            header('location:index.php');
        } else {
           
            $_SESSION['termerr'] = "Please Check Term and Condition";
            header('location:create.php');
        }
    }

    public function index() {
        $data = array();
        $query = "SELECT * FROM term_and_condition WHERE deleted_at IS NULL";
        $result = mysql_query($query);

        while ($onerecord = mysql_fetch_assoc($result)) {
            $data[] = $onerecord;
        }
        return $data;
    }

    public function view($id = '') {
        $query = "SELECT * FROM term_and_condition WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function delete($id = '') {
        $query = "DELETE FROM term_and_condition  WHERE id=" . $id;
        mysql_query($query);

        utility::message("Permanently Deleted!");
        header('location:index.php');
    }

    public function update() {
        //  echo '<pre>';
        // print_r($this->id);
        //  exit();
        $query = "UPDATE term_and_condition SET term_and_condition='" . $this->term_and_condition . "'  WHERE id=$this->id";
        mysql_query($query);
        //  echo '<pre>';
        // print_r($r);
        //  exit();
        utility::message("Updated Successfully!");
        header('location:index.php');
    }

    public function trash($id = '') {
        $query = "UPDATE term_and_condition SET deleted_at = '" . date('Y-m-d') . "' WHERE id=" . $id;
        mysql_query($query);

        utility::message("Deleted Successfully!");
        header('location:index.php');
    }

    public function trashed() {
        $data = array();
        $query = "SELECT * FROM term_and_condition WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);

        while ($onerecord = mysql_fetch_assoc($result)) {
            $data[] = $onerecord;
        }
        return $data;
    }

    public function restore($id = '') {
        $query = "UPDATE term_and_condition SET deleted_at = NULL WHERE id=" . $id;
        mysql_query($query);

        utility::message("Restore Successfully!");
        header('location:index.php');
    }

}
