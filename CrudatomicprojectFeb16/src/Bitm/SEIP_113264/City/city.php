<?php
namespace App\Bitm\SEIP_113264\City;

use App\Bitm\SEIP_113264\Abstract_class\abstract_class;
use App\Bitm\SEIP_113264\Utility\utility;

class city extends abstract_class {
    protected $table = 'tbl_city';
    public $id = '';
    public $name = '';
    public $city = '';
    
    
     public function prepare_data($data = '') {
       //  print_r($data);
       //  exit();
        if(array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->name = $data['name'];
        }else{
             utility::message("Please Insert a Name!");
             header('location:create.php');
        }
        if(array_key_exists('city', $data) && !empty($data['city'])){
            $this->city = $data['city'];
        }
    }
    
    public function insert(){
      
        $sql = "INSERT INTO $this->table(name, city)VALUES(:name, :city);";
        
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':city', $this->city);
        
        
        $stmt->execute();
        utility::message("Saved Successfully!");
        header('location:index.php');
    }
    
    public function update(){
        
        if(!empty($this->city)){
            $sql = "UPDATE $this->table SET name = :name, city = :city WHERE id=:id";
            $q=$this->pdo->prepare($sql);
            $q->execute(array(
                ':name' => $this->name,
                ':city' => $this->city,
                ':id' => $this->id,
            ));
        }else{
             $sql = "UPDATE $this->table SET name = :name WHERE id=:id";
            $q=$this->pdo->prepare($sql);
            $q->execute(array(
                ':name' => $this->name,
                ':id' => $this->id,
            ));
        }
        if($q){
            utility::message("Updated Successfully!");
             header('location:index.php');
        }else{
            utility::message("Errors!");
             header('location:edit.php?id='.$this->id);
        }
        
    }
    
    
    
    
    
    
}
