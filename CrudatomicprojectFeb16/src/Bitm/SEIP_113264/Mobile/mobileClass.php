<?php
namespace App\Bitm\SEIP_113264\Mobile;

session_start();
use App\Bitm\SEIP_113264\Utility\utility;

class mobileClass{
    
    public function __construct(){
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to Connect with Server!');
        mysql_select_db('db_crudprojectfeb16', $conn) or die('Unable to Select Database');
    }
    
    public function store($data = ''){
//        echo '<pre>';
//        print_r($data);
//        exit();
        $title = $data['title'];
        $model = $data['model'];
      
        if(!empty($title) && !empty($model)){
        $query = "INSERT INTO tbl_mobiles (title, model) VALUES ('".$title."', '".$model."')";
        mysql_query($query);

        utility::message("Saved Successfully!");
        header('location:index.php');
        }else{
            $_SESSION['titlerr'] = "Please Enter a Title";
            $_SESSION['modelerr'] = "Please Enter a Model";
            
            header('location:create.php');
        }
        
        
    }
    
    public function view(){
        $data = array();
        $query = "SELECT * FROM tbl_mobiles WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($onerecord = mysql_fetch_assoc($result)){
            $data[]= $onerecord;
        }
        return $data;
    }
    
    public function show($id = ''){
        $query = "SELECT * FROM tbl_mobiles WHERE id=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
    public function update($data = ''){
        $id = $data['id'];
        $title = $data['title'];
        $model = $data['model'];
        $image = $data['m_image'];
        
        $query = "UPDATE tbl_mobiles SET title='".$title."', model='".$model."', m_image ='".$image."', updated_at='".date('Y-m-d H:i:s')."' WHERE id=$id";
        mysql_query($query);
        
        utility::message("Updated Successfully!");
        header('location:index.php');
    }
    
    public function delete($id = ''){
        $query = "DELETE FROM tbl_mobiles WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Deleted Permanently!");
        header('location:trashed.php');
    }
    
    public function trash($id = ''){
        
        $query = "UPDATE tbl_mobiles SET deleted_at='".date('Y-m-d H:g:i')."' WHERE id=$id";
        mysql_query($query);
        
        utility::message("Deleted Successfully!");
        header('location:index.php');
    }
    
     public function trashed(){
        $data = array();
        $query = "SELECT * FROM tbl_mobiles WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($onerecord = mysql_fetch_assoc($result)){
            $data[]= $onerecord;
        }
        return $data;
    }
    
     public function restore($id = ''){
        
        $query = "UPDATE tbl_mobiles SET deleted_at=NULL WHERE id=$id";
        mysql_query($query);
        
        utility::message("Restore Successfully!");
        header('location:index.php');
    }
    
    
}
