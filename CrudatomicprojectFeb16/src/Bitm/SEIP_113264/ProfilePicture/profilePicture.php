<?php

namespace App\Bitm\SEIP_113264\ProfilePicture;

//session_start();
use App\Bitm\SEIP_113264\Utility\utility;
use PDO;

class profilePicture {
    
    public $id = '';
    public $pdo = '';
    public $table='profilePictures';
    public $name='';
    public $profile_pic='';

    
    
    public function __construct() {
        try {
            $this->pdo = new PDO("mysql:host=localhost;dbname=db_crudprojectfeb16", "root", "");
             $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
        } catch (PDOException $e) {
            echo 'Connection failed! ' . $e->getMessage();
        }
       //  $this->pdo = $conn;
    }

    public function prepare_data($data = '') {

        if(array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->name = $data['name'];
        }
        if(array_key_exists('profile_pic', $data) && !empty($data['profile_pic'])){
            $this->profile_pic = $data['profile_pic'];
        }
    }
    
    public function insert(){
      
        $sql = "INSERT INTO $this->table(name, profile_pic)VALUES(:name, :profile_pic);";
        
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':profile_pic', $this->profile_pic);
        
        
        $stmt->execute();
        utility::message("Saved Successfully!");
        header('location:index.php');
    }
    
    public function index(){
        $data = array();
        $sql = "SELECT * FROM $this->table WHERE deleted_at IS NULL";
        $q = $this->pdo->query($sql);
        
        while($row = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function view($id = ''){
        $sql = "SELECT * FROM $this->table WHERE id = :id";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':id' => $id));
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
        
    }
    
    public function update(){
        
        if(!empty($this->profile_pic)){
            $sql = "UPDATE $this->table SET name = :name, profile_pic = :profile_pic WHERE id=:id";
            $q=$this->pdo->prepare($sql);
            $q->execute(array(
                ':name' => $this->name,
                ':profile_pic' => $this->profile_pic,
                ':id' => $this->id,
            ));
        }else{
             $sql = "UPDATE $this->table SET name = :name WHERE id=:id";
            $q=$this->pdo->prepare($sql);
            $q->execute(array(
                ':name' => $this->name,
                ':id' => $this->id,
            ));
        }
        if($q){
            utility::message("Updated Successfully!");
             header('location:index.php');
        }else{
            utility::message("Errors!");
             header('location:edit.php?id='.$this->id);
        }
        
    }
    
    public function Profile_Picture(){
        $sql = "SELECT * FROM $this->table WHERE status = :status";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':status' => 1));
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
        
    }
    
    public function status_active($id = ''){
        $sql = "UPDATE $this->table SET status = :active WHERE id=:id";
        $q = $this->pdo->prepare($sql);
//        $q->bindParam(':id', $id);
//        $q->bindParam(':active', 1);
        
        $q->execute(array(':active' => 1, ':id'=> $id));
        utility::message("Profile Pic Saved Successfully!");
        header('location:index.php');
    }
    public function status_deactive($id = ''){
        $sql = "UPDATE $this->table SET status = :deactive WHERE id=:id";
        $q = $this->pdo->prepare($sql);

        $q->execute(array(':deactive' => 0, ':id'=> $id));
        utility::message("Profile Pic Deactivate Successfully!");
        header('location:index.php');
    }
    public function trash($id = ''){
        $sql = "UPDATE $this->table SET deleted_at = :date WHERE id=:id";
        $q = $this->pdo->prepare($sql);

        $q->execute(array(':date' => date('Y-m-d'), ':id'=> $id));
        utility::message("Deleted Successfully!");
        header('location:index.php');
    }
    
    public function trashed(){
        $data = array();
        $sql = "SELECT * FROM $this->table WHERE deleted_at IS NOT NULL";
        $q = $this->pdo->query($sql);
        
        while($row = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function restore($id = ''){
        $sql = "UPDATE $this->table SET deleted_at = :date WHERE id=:id";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':date' =>NULL, ':id'=> $id));
        utility::message("Restore Successfully!");
        header('location:index.php');
    }
    
    public function delete($id = ''){
        $sql = "DELETE FROM $this->table WHERE id=:id";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':id' => $id));
//         print_r($id);
//        exit();
        utility::message("Deleted Permanently!");
        header('location:trashed.php');
    }
    
    
    
    

}
