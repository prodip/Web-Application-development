<?php
namespace App\Bitm\SEIP_113264\Abstract_class;
session_start();
use PDO;
use App\Bitm\SEIP_113264\Utility\utility;

abstract class abstract_class {
    protected $table;

    public function __construct() {
        try {
            $this->pdo = new PDO("mysql:host=localhost;dbname=db_crudprojectfeb16", "root", "");
             $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
        } catch (PDOException $e) {
            echo 'Connection failed! ' . $e->getMessage();
        }
        
    }
    
    abstract public function prepare_data($data = '');
    abstract public function insert();
    abstract public function update();
     
     
     
     public function index($page = 0, $r = 5){
        $data = array();
        $sql = "SELECT * FROM $this->table WHERE deleted_at IS NULL LIMIT $page,$r";
        $q = $this->pdo->query($sql);
        
        while($row = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function view($id = ''){
        $sql = "SELECT * FROM $this->table WHERE id = :id";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':id' => $id));
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
        
    }
     
    
    public function Profile_Picture(){
        $sql = "SELECT * FROM $this->table WHERE status = :status";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':status' => 1));
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
        
    }
    
    public function status_active($id = ''){
        $sql = "UPDATE $this->table SET status = :active WHERE id=:id";
        $q = $this->pdo->prepare($sql);
//        $q->bindParam(':id', $id);
//        $q->bindParam(':active', 1);
        
        $q->execute(array(':active' => 1, ':id'=> $id));
        utility::message("Profile Pic Saved Successfully!");
        header('location:index.php');
    }
    public function status_deactive($id = ''){
        $sql = "UPDATE $this->table SET status = :deactive WHERE id=:id";
        $q = $this->pdo->prepare($sql);

        $q->execute(array(':deactive' => 0, ':id'=> $id));
        utility::message("Profile Pic Deactivate Successfully!");
        header('location:index.php');
    }
    public function trash($id = ''){
        $sql = "UPDATE $this->table SET deleted_at = :date WHERE id=:id";
        $q = $this->pdo->prepare($sql);

        $q->execute(array(':date' => date('Y-m-d'), ':id'=> $id));
        utility::message("Deleted Successfully!");
        header('location:index.php');
    }
    
    public function trashed(){
        $data = array();
        $sql = "SELECT * FROM $this->table WHERE deleted_at IS NOT NULL";
        $q = $this->pdo->query($sql);
        
        while($row = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function restore($id = ''){
        $sql = "UPDATE $this->table SET deleted_at = :date WHERE id=:id";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':date' =>NULL, ':id'=> $id));
        utility::message("Restore Successfully!");
        header('location:index.php');
    }
    
    public function delete($id = ''){
        $sql = "DELETE FROM $this->table WHERE id=:id";
        $q = $this->pdo->prepare($sql);
        $q->execute(array(':id' => $id));
        
        utility::message("Deleted Permanently!");
        header('location:trashed.php');
    }
     
    public function pagination(){
        $sql = "SELECT * FROM $this->table WHERE deleted_at IS NULL";
        $pagi = $this->pdo->prepare($sql);
        $pagi->execute();
     
        return $cout = $pagi->rowCount();
    } 
     
     
    
    
}
