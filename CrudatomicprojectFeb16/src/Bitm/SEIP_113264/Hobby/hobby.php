<?php

namespace App\Bitm\SEIP_113264\Hobby;
session_start();
use App\Bitm\SEIP_113264\Utility\utility;

class hobby{
    
    public $id = '';
    public $name ='';
    public $hobbies = '';
    
   public function __construct(){
       $conn = mysql_connect('localhost','root','') or die("Unable to connect with Server");
       mysql_select_db('db_crudprojectfeb16', $conn) or die("Unable to select Database");
   }
    
    public function prepare($data = ''){
       // print_r($data);
       // die();
        if(isset($data['id']) && !empty($data['id'])){
        $this->id = $data['id'];
        }
        
        if(isset($data['name']) && !empty($data['name'])){
        $this->name = $data['name'];
        }else{
            $_SESSION['namerr'] = 'Please enter a Name';
            header('location:create.php');
        }
        $hobbies = $data['hobby'];
        $itmes = implode(',', $hobbies);
        $data['hobby']=$itmes;
     //   print_r($data['hobby']);
         if(isset($data['hobby']) && !empty($data['hobby'])){
        $this->hobbies = $data['hobby'];
        }else{
            $_SESSION['hobbyerr'] = 'Please Select  Hobby';
            header('location:create.php');
        }
    }
    
    public function store(){
        
        $query = "INSERT INTO hobbies(name, hobby) VALUES('".$this->name."','".$this->hobbies."')";
        mysql_query($query);
        
        utility::message('Save Successfully!');
        header('location:index.php');
        
    }
    
    public function index(){
        $data = array();
        $query = "SELECT * FROM hobbies WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function view($id = ''){
        $query ="SELECT * FROM hobbies WHERE id=".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
    public function update(){
       // print_r($this->id);
       // die();
        $query = "UPDATE hobbies SET name='".$this->name."', hobby='".$this->hobbies."' WHERE id=".$this->id;
       // print_r($query);
       // die();
        mysql_query($query);
        
        utility::message("Updated Successfully!");
        header('location:index.php');
    }
    
    public function trash($id = ''){
        $query = "UPDATE hobbies SET deleted_at='".date('Y-m-d')."' WHERE id=".$id;
//        print_r($query);
//        die();
        mysql_query($query);
        
        utility::message("Deleted Successfully!");
        header('location:index.php');
    }
    
    public function trashed(){
        $data = array();
        $query = "SELECT * FROM hobbies WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function restore($id = ''){
        $query = "UPDATE hobbies SET deleted_at=NULL WHERE id=".$id;
//        print_r($query);
//        die();
        mysql_query($query);
        
        utility::message("Restore Successfully!");
        header('location:index.php');
    }
    
    public function delete($id = ''){
        $query = "DELETE FROM hobbies WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Permanently Deleted!");
        header('location:trashed.php');
    }
    
    
}

