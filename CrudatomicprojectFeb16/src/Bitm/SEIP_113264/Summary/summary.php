<?php
namespace App\Bitm\SEIP_113264\Summary;


use App\Bitm\SEIP_113264\Utility\utility;


class summary {
    public $id = '';
    public $name = '';
    public $summary = '';
    
    public function __construct(){
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to Connect with Server!');
        mysql_select_db('db_crudprojectfeb16', $conn) or die('Unable to Select Database');
    }
    
    public function prepare($data = ''){
     //    echo '<pre>';
     //   print_r($data);
    //   exit();
        if(isset($data['id'])){
            $this->id = $data['id'];
        }
            $this->name = $data['name'];
            $this->summary = $data['summary'];
            
        
    }
    
    
    
    public function store(){
//        print_r( $this->email_address);
//       exit();
        
        
       if( !empty($this->name) && !empty($this->summary)){
        $query = "INSERT INTO `db_crudprojectfeb16`.`summaries` (`id`, `name`, `summary`) VALUES (NULL, '".$this->name."', '".$this->summary."');";
      //  print_r( $query);
     //  exit();
        mysql_query($query);
   
        utility::message("Saved Successfully!");
        header('location:index.php');
       } else{
            $_SESSION['namerr'] = "Please Enter a Name";
            $_SESSION['emailerr'] = "Please Select a Gender";
              header('location:create.php');
        }
}

    public function index(){
        $data = array();
        $query = "SELECT * FROM summaries WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($onerecord = mysql_fetch_assoc($result)){
            $data[] = $onerecord;
        }
        return $data;
        
    }

    public function view($id = ''){
        $query = "SELECT * FROM summaries WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        return $row;
        
    }
    
     public function delete($id = ''){
        $query = "DELETE FROM summaries  WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Permanently Deleted!");
        header('location:index.php');
    }

    public function update(){
      //  echo '<pre>';
      // print_r($this->id);
      //  exit();
         $query = "UPDATE summaries SET name='".$this->name."', summary='".$this->summary."' WHERE id=$this->id";
       mysql_query($query);
      //  echo '<pre>';
      // print_r($r);
      //  exit();
        utility::message("Updated Successfully!");
        header('location:index.php');
    }
    
    public function trash($id = ''){
        $query = "UPDATE summaries SET deleted_at = '".date('Y-m-d')."' WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Deleted Successfully!");
        header('location:index.php');
    }


    public function trashed(){
        $data = array();
        $query = "SELECT * FROM summaries WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($onerecord = mysql_fetch_assoc($result)){
            $data[] = $onerecord;
        }
        return $data;
        
    }
    
    public function restore($id = ''){
        $query = "UPDATE summaries SET deleted_at = NULL WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Restore Successfully!");
        header('location:index.php');
    }
    
}
