<?php
namespace App\Bitm\SEIP_113264\Book;

//session_start();
use App\Bitm\SEIP_113264\Abstract_class\abstract_class;
use App\Bitm\SEIP_113264\Utility\utility;


class BookClass_File extends abstract_class {
    
    public $id = '';
    public $title = '';
    public $author_name = '';
    public $book_cover_image = '';
    protected $table = 'books';
    
     public function prepare_data($data = '') {
//         echo '<pre>';
//         print_r($data);
//         exit();
        if(array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if(array_key_exists('title', $data) && !empty($data['title'])){
            $this->title = $data['title'];
        }else{
             utility::message("Please Insert a title!");
             header('location:create.php');
        }
        if(array_key_exists('author_name', $data) && !empty($data['author_name'])){
            $this->author_name = $data['author_name'];
        }
        if(array_key_exists('book_image', $data) && !empty($data['book_image'])){
            $this->book_cover_image = $data['book_image'];
        }
    }
    
    public function insert(){
      
        $sql = "INSERT INTO $this->table(title, author_name, book_image)VALUES(:title, :author_name, :book_cover_image);";
        
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':author_name', $this->author_name);
        $stmt->bindParam(':book_cover_image', $this->book_cover_image);
        
        
        $stmt->execute();
        utility::message("Saved Successfully!");
        header('location:index.php');
    }
    
    public function update(){
        
        if(!empty($this->book_cover_image)){
            $sql = "UPDATE $this->table SET title = :title, author_name = :author_name, book_image = :book_cover_image WHERE id=:id";
            $q=$this->pdo->prepare($sql);
            $q->execute(array(
                ':title' => $this->title,
                ':author_name' => $this->author_name,
                ':book_cover_image' => $this->book_cover_image,
                ':id' => $this->id,
            ));
        }else{
             $sql = "UPDATE $this->table SET title = :title, author_name = :author_name WHERE id=:id";
            $q=$this->pdo->prepare($sql);
            $q->execute(array(
                ':title' => $this->title,
                ':author_name' => $this->author_name,
                ':id' => $this->id,
            ));
        }
        if($q){
            utility::message("Updated Successfully!");
             header('location:index.php');
        }else{
            utility::message("Errors!");
             header('location:edit.php?id='.$this->id);
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
    
    
    
    
}
