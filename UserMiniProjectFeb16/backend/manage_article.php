<?php
if (!isset($_SESSION)) {
    session_start();
}
if ($_SESSION['user_id']) {
    
} else {
    header('location:../index.php');
}
?>
<?php
include_once '../vendor/autoload.php';

use App\Users;
use App\Profiles;
use App\Articles;

$articleobj = new Articles();
$profileobj = new Profiles();
$userobj = new Users();
$all_users = $userobj->view_all_users();

$user_all_info = $profileobj->view_all_users();

$article_info = $articleobj->manage_article();
//echo '<pre>';
//print_r($article_info);
//exit();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SuperheroAdmin - Bootstrap Admin Template</title>

        <!-- bootstrap -->
        <link href="../css/bootstrap/bootstrap.css" rel="stylesheet" />

        <!-- libraries -->
        <!-- <link href="css/libs/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" /> -->
        <link href="../css/libs/font-awesome.css" type="text/css" rel="stylesheet" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="../css/compiled/layout.css">
        <link rel="stylesheet" type="text/css" href="../css/compiled/elements.css">

        <!-- this page specific styles -->
        <link rel="stylesheet" href="../css/libs/fullcalendar.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/fullcalendar.print.css" type="text/css" media="print" />
        <link rel="stylesheet" href="../css/compiled/calendar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="../css/libs/morris.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/daterangepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/jquery-jvectormap-1.2.2.css" type="text/css" />

        <!-- Favicon -->
        <link type="image/x-icon" href="../favicon.png" rel="shortcut icon"/>

        <!-- google font libraries -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>
        <style type="text/css">
            a:hover{ text-decoration: none !important;}
        </style>

    </head>
    <body>
        <?php include_once 'admin_header_bar.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-2" id="nav-col">
                    <?php include_once 'admin_left_sidebar.php'; ?>
                </div>
                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="clearfix">
                                <h2 class="pull-left">Manage Articles</h2>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="table-responsive">
                                            <table class="table user-list">
                                                <thead>
                                                    <tr>
                                                        <th><span>Posted By</span></th>
                                                        <th><span>Article Title</span></th>
                                                        <th class="text-center"><span>Publication Status</span></th>
                                                        <th><span>Posted Date</span></th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    foreach ($article_info as $v_article_info) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php if (isset($v_article_info['image']) && !empty($v_article_info['image'])) { ?>
                                                                    <img src="../img/profile_pics/<?php echo $v_article_info['image']; ?>" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>
                                                                <?php } else { ?>
                                                                    <img src="../img/samples/Unknown_Person.png" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>

                                                                <?php } ?>
                                                                <a href="user_view.php?id=<?php echo $v_article_info['id']; ?>" class="user-link"><?php echo $v_article_info['firstname'] . ' ' . $v_article_info['lastname']; ?></a>
                                                                <span class="user-subhead">
                                                                    <?php
                                                                    if ($v_article_info['is_admin'] == 1) {
                                                                        echo "Admin";
                                                                    } else {
                                                                        echo "User";
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <?php echo $v_article_info['title']; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($v_article_info['publication_status'] == 1) { ?>
                                                                <a href = "article_unpublished.php?id=<?php echo $v_article_info['article_id']; ?>">
                                                                        <span class = "label label-danger">Unpublised</span>
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <a href = "article_published.php?id=<?php echo $v_article_info['article_id']; ?>">
                                                                        <span class = "label label-success">Published</span>
                                                                    </a>
                                                                <?php } ?>

                                                            </td>
                                                            <td style="width: 10%;">
                                                                <?php
                                                                $data = $v_article_info['created_at'];
                                                                $date_format = date('d M Y H:i A', strtotime($data));
                                                                echo $date_format;
                                                                ?>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="user_view.php?id=<?php echo $v_article_info['article_id']; ?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="article_edit.php?id=<?php echo $v_article_info['article_id']; ?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="article_delete.php?id=<?php echo $v_article_info['article_id']; ?>" class="table-link danger">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                        <ul class="pagination pull-right">
                                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer-bar">
            <p id="footer-copyright">
                &copy; 2014 <a href="http://www.adbee.sk/" target="_blank">Adbee digital</a>. Powered by SuperheroAdmin.
            </p>
        </footer>

        <!-- global scripts -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.js"></script>

        <!-- this page specific scripts -->


        <!-- theme scripts -->
        <script src="../js/scripts.js"></script>

        <!-- this page specific inline scripts -->

    </body>
</html>
