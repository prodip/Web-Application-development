<?php
if (!isset($_SESSION)) {
    session_start();
}

//echo '<pre>';
//print_r($_SESSION);
//exit();

if ($_SESSION['user_id']) {
    
} else {
    header('location:../index.php');
}

include_once '../vendor/autoload.php';

use App\Users;
use App\Profiles;

$profileobj = new Profiles();
$userobj = new Users();
$user_id = $_SESSION['user_id'];
$id = $_GET['id'];
$user_info = $userobj->view_user($user_id);
$user_profile_info = $profileobj->view_user_details($user_id);
$user_view = $profileobj->view_user_details_by_id($id);

/*
echo '<pre>';
print_r($user_view);
exit();
 */
 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SuperheroAdmin - Bootstrap Admin Template</title>

        <!-- bootstrap -->
        <link href="../css/bootstrap/bootstrap.css" rel="stylesheet" />

        <!-- libraries -->
        <!-- <link href="css/libs/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" /> -->
        <link href="../css/libs/font-awesome.css" type="text/css" rel="stylesheet" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="../css/compiled/layout.css">
        <link rel="stylesheet" type="text/css" href="../css/compiled/elements.css">

        <!-- this page specific styles -->
        <link rel="stylesheet" href="../css/libs/fullcalendar.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/fullcalendar.print.css" type="text/css" media="print" />
        <link rel="stylesheet" href="../css/compiled/calendar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="../css/libs/morris.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/daterangepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/jquery-jvectormap-1.2.2.css" type="text/css" />

        <!-- Favicon -->
        <link type="image/x-icon" href="../favicon.png" rel="shortcut icon"/>

        <!-- google font libraries -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>


    </head>
    <body>
     <?php include_once 'admin_header_bar.php';?>
        <div class="container">
            <div class="row">
                <div class="col-md-2" id="nav-col">
                  <?php include_once 'admin_left_sidebar.php';?>
                </div>
                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">

                            <h1>
                                <?php
                                if ($user_info['is_admin'] == 1) {
                                    echo 'Admin';
                                } else {
                                    echo 'User';
                                }
                                ?> Profile
                            </h1>
                            <div class="row" id="user-profile">
                                <div class="col-lg-3 col-md-4 col-sm-4">
                                    <div class="main-box clearfix">
                                        <h2 style="text-transform: capitalize;">
                                            <?php
                                            if (isset($user_view['firstname'], $user_profile_info['lastname']) && !empty($user_view['firstname'])) {
                                                echo $user_view['firstname'].' '.$user_view['lastname'];
                                            } else {
                                                echo $user_info['username'];
                                            }
                                            ?>
                                        </h2>

                                        <div class="profile-status">
                                            <i class="fa fa-check-circle"></i> Online
                                        </div>
                                        <?php if (isset($user_view['image']) && !empty($user_view['image'])) { ?>
                                            <img src="../img/profile_pics/<?php echo $user_view['image']; ?>" width="159" height="159" alt="" class="profile-img img-responsive center-block"/>
                                        <?php } else { ?>
                                            <img src="../img/samples/Unknown_Person.png" width="159" height="159" alt="" class="profile-img img-responsive center-block"/>
                                            
                                        <?php } ?>


                                        <div class="profile-label">
                                            <span class="label label-danger">
                                                <?php
                                                if ($user_info['is_admin'] == 1) {
                                                    echo 'Admin';
                                                } else {
                                                    echo 'User';
                                                }
                                                ?>
                                            </span>
                                        </div>

                                        <div class="profile-stars">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <span>Super User</span>
                                        </div>

                                        <div class="profile-since">
                                            <?php
                                            if ($user_info['is_admin'] == 1) {
                                                echo 'Admin';
                                            } else {
                                                echo 'Member';
                                            }
                                            $created_date = $user_info['created_at'];
                                            ?>
                                            since: <?php echo date('M Y', strtotime($created_date)); ?>
                                        </div>

                                        <div class="profile-details">
                                            <ul class="fa-ul">
                                                <li><i class="fa-li fa fa-truck"></i>Orders: <span>456</span></li>
                                                <li><i class="fa-li fa fa-comment"></i>Posts: <span>828</span></li>
                                                <li><i class="fa-li fa fa-tasks"></i>Tasks done: <span>1024</span></li>
                                            </ul>
                                        </div>

                                        <div class="profile-message-btn center-block text-center">
                                            <a href="#" class="btn btn-success">
                                                <i class="fa fa-envelope"></i>
                                                Send message
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-9 col-md-8 col-sm-8">
                                    <span style="text-align: center; color: green; font-size: 18px">
                                        <?php
                                        if (array_key_exists('profile_save_mgs', $_SESSION) && !empty($_SESSION['profile_save_mgs'])) {
                                            echo $_SESSION['profile_save_mgs'];
                                            unset($_SESSION['profile_save_mgs']);
                                        }
                                        ?>
                                    </span>
                                    <div class="main-box clearfix">
                                        <div class="profile-header">
                                            <h3>
                                                <span>
                                                    <?php
                                                    if ($user_info['is_admin'] == 1) {
                                                        echo 'Admin';
                                                    } else {
                                                        echo 'User';
                                                    }
                                                    ?> info
                                                </span>
                                            </h3>
                                            <a href="edit_profile.php?id=<?php echo $user_info['id']; ?>" class="btn btn-primary edit-profile">
                                                <i class="fa fa-pencil-square fa-lg"></i> 
                                                Edit profile
                                            </a>
                                        </div>

                                        <div class="row profile-user-info">
                                            <div class="col-sm-8">
                                                <div class="profile-user-details clearfix">
                                                    <div class="profile-user-details-label">
                                                        First Name
                                                    </div>
                                                    <div class="profile-user-details-value" style="text-transform: capitalize;">
                                                        <?php 
                                                            if(isset($user_view['firstname']) && !empty($user_view['firstname'])){
                                                                echo $user_view['firstname'];
                                                            }else{
                                                                echo '..................';
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="profile-user-details clearfix">
                                                    <div class="profile-user-details-label">
                                                        Last Name
                                                    </div>
                                                    <div class="profile-user-details-value" style="text-transform: capitalize;">
                                                        <?php 
                                                            if(isset($user_view['lastname']) && !empty($user_view['lastname'])){
                                                                echo $user_view['lastname'];
                                                            }else{
                                                                echo '..................';
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="profile-user-details clearfix">
                                                    <div class="profile-user-details-label">
                                                        Address
                                                    </div>
                                                    <div class="profile-user-details-value">
                                                        <?php 
                                                            if(isset($user_view['address']) && !empty($user_view['address'])){
                                                                echo $user_view['address'];
                                                            }else{
                                                                echo '..................<br>
                                                                      ..............';
                                                            }
                                                        ?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="profile-user-details clearfix">
                                                    <div class="profile-user-details-label">
                                                        Email ID
                                                    </div>
                                                    <div class="profile-user-details-value">
                                                        <?php 
                                                            if(isset($user_view['email']) && !empty($user_view['email'])){
                                                                echo $user_view['email'];
                                                            }else{
                                                                echo $user_info['email'];
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="profile-user-details clearfix">
                                                    <div class="profile-user-details-label">
                                                        Mobile No
                                                    </div>
                                                    <div class="profile-user-details-value">
                                                         <?php 
                                                            if(isset($user_view['mobile_no']) && !empty($user_view['mobile_no'])){
                                                                echo $user_view['mobile_no'];
                                                            }else{
                                                                echo '..................';
                                                            }
                                                        ?>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 profile-social">
                                                <ul class="fa-ul">
                                                    <li style="text-transform: lowercase;"><i class="fa-li fa fa-twitter-square"></i><a href="#">@<?php echo $user_profile_info['firstname'].$user_profile_info['lastname']?></a></li>
                                                    <li><i class="fa-li fa fa-linkedin-square"></i>
                                                        <a href="#">
                                                        <?php if(isset($user_view['firstname'])){ 
                                                            echo $user_view['firstname'].' '.$user_view['lastname'];
                                                        }else {
                                                            echo '____________';
                                                        }
                                                        ?>
                                                        </a>
                                                    </li>
                                                    <li><i class="fa-li fa fa-facebook-square"></i>
                                                        <a href="#">
                                                            <?php if(isset($user_view['firstname'])){ 
                                                            echo $user_view['firstname'].' '.$user_view['lastname'];
                                                        }else {
                                                            echo '____________';
                                                        }
                                                        ?>
                                                        </a>
                                                    </li>
                                                    <li><i class="fa-li fa fa-skype"></i><a href="#">Black_widow</a></li>
                                                    <li><i class="fa-li fa fa-instagram"></i><a href="#">Avenger_Scarlett</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="tabs-wrapper profile-tabs">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#tab-activity" data-toggle="tab">Activity</a></li>
                                                <li><a href="#tab-friends" data-toggle="tab">Friends</a></li>
                                                <li><a href="#tab-chat" data-toggle="tab">Chat</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane fade in active" id="tab-activity">

                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-comment"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson posted a comment in <a href="#">Avengers Initiative</a> project.
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-truck"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson changed order status from <span class="label label-primary">Pending</span>
                                                                        to <span class="label label-success">Completed</span>
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-check"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson posted a comment in <a href="#">Lost in Translation opening scene</a> discussion.
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-users"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson posted a comment in <a href="#">Avengers Initiative</a> project.
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-heart"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson changed order status from <span class="label label-warning">On Hold</span>
                                                                        to <span class="label label-danger">Disabled</span>
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-check"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson posted a comment in <a href="#">Lost in Translation opening scene</a> discussion.
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-truck"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson changed order status from <span class="label label-primary">Pending</span>
                                                                        to <span class="label label-success">Completed</span>
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <i class="fa fa-users"></i>
                                                                    </td>
                                                                    <td>
                                                                        Scarlett Johansson posted a comment in <a href="#">Avengers Initiative</a> project.
                                                                    </td>
                                                                    <td>
                                                                        2014/08/08 12:08
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>

                                                <div class="tab-pane fade" id="tab-friends">
                                                    <ul class="widget-users row">
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/scarlet.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Scarlett Johansson</a>
                                                                </div>
                                                                <div class="time">
                                                                    <i class="fa fa-clock-o"></i> Last online: 5 minutes ago
                                                                </div>
                                                                <div class="type">
                                                                    <span class="label label-danger">Admin</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/kunis.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Mila Kunis</a>
                                                                </div>
                                                                <div class="time online">
                                                                    <i class="fa fa-check-circle"></i> Online
                                                                </div>
                                                                <div class="type">
                                                                    <span class="label label-warning">Pending</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/ryan.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Ryan Gossling</a>
                                                                </div>
                                                                <div class="time online">
                                                                    <i class="fa fa-check-circle"></i> Online
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/robert.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Robert Downey Jr.</a>
                                                                </div>
                                                                <div class="time">
                                                                    <i class="fa fa-clock-o"></i> Last online: Thursday
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/emma.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Emma Watson</a>
                                                                </div>
                                                                <div class="time">
                                                                    <i class="fa fa-clock-o"></i> Last online: 1 week ago
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/george.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">George Clooney</a>
                                                                </div>
                                                                <div class="time">
                                                                    <i class="fa fa-clock-o"></i> Last online: 1 month ago
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/kunis.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Mila Kunis</a>
                                                                </div>
                                                                <div class="time online">
                                                                    <i class="fa fa-check-circle"></i> Online
                                                                </div>
                                                                <div class="type">
                                                                    <span class="label label-warning">Pending</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-6">
                                                            <div class="img">
                                                                <img src="../img/samples/ryan.png" alt=""/>
                                                            </div>
                                                            <div class="details">
                                                                <div class="name">
                                                                    <a href="#">Ryan Gossling</a>
                                                                </div>
                                                                <div class="time online">
                                                                    <i class="fa fa-check-circle"></i> Online
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <br/>
                                                    <a href="#" class="btn btn-success pull-right">View all users</a>
                                                </div>

                                                <div class="tab-pane fade" id="tab-chat">
                                                    <div class="conversation-wrapper">
                                                        <div class="conversation-content">
                                                            <div class="conversation-inner">

                                                                <div class="conversation-item item-left clearfix">
                                                                    <div class="conversation-user">
                                                                        <img src="../img/samples/ryan.png" alt=""/>
                                                                    </div>
                                                                    <div class="conversation-body">
                                                                        <div class="name">
                                                                            Ryan Gossling
                                                                        </div>
                                                                        <div class="time hidden-xs">
                                                                            September 21, 2013 18:28
                                                                        </div>
                                                                        <div class="text">
                                                                            I don't think they tried to market it to the billionaire, spelunking, 
                                                                            base-jumping crowd.
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="conversation-item item-right clearfix">
                                                                    <div class="conversation-user">
                                                                        <img src="../img/samples/kunis.png" alt=""/>
                                                                    </div>
                                                                    <div class="conversation-body">
                                                                        <div class="name">
                                                                            Mila Kunis
                                                                        </div>
                                                                        <div class="time hidden-xs">
                                                                            September 21, 2013 12:45
                                                                        </div>
                                                                        <div class="text">
                                                                            Normally, both your asses would be dead as fucking fried chicken, but you 
                                                                            happen to pull this shit while I'm in a transitional period so I don't wanna 
                                                                            kill you, I wanna help you.
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="conversation-item item-right clearfix">
                                                                    <div class="conversation-user">
                                                                        <img src="../img/samples/kunis.png" alt=""/>
                                                                    </div>
                                                                    <div class="conversation-body">
                                                                        <div class="name">
                                                                            Mila Kunis
                                                                        </div>
                                                                        <div class="time hidden-xs">
                                                                            September 21, 2013 12:45
                                                                        </div>
                                                                        <div class="text">
                                                                            Normally, both your asses would be dead as fucking fried chicken, but you 
                                                                            happen to pull this shit while I'm in a transitional period so I don't wanna 
                                                                            kill you, I wanna help you.
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="conversation-item item-left clearfix">
                                                                    <div class="conversation-user">
                                                                        <img src="../img/samples/ryan.png" alt=""/>
                                                                    </div>
                                                                    <div class="conversation-body">
                                                                        <div class="name">
                                                                            Ryan Gossling
                                                                        </div>
                                                                        <div class="time hidden-xs">
                                                                            September 21, 2013 18:28
                                                                        </div>
                                                                        <div class="text">
                                                                            I don't think they tried to market it to the billionaire, spelunking, 
                                                                            base-jumping crowd.
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="conversation-item item-right clearfix">
                                                                    <div class="conversation-user">
                                                                        <img src="../img/samples/kunis.png" alt=""/>
                                                                    </div>
                                                                    <div class="conversation-body">
                                                                        <div class="name">
                                                                            Mila Kunis
                                                                        </div>
                                                                        <div class="time hidden-xs">
                                                                            September 21, 2013 12:45
                                                                        </div>
                                                                        <div class="text">
                                                                            Normally, both your asses would be dead as fucking fried chicken, but you 
                                                                            happen to pull this shit while I'm in a transitional period so I don't wanna 
                                                                            kill you, I wanna help you.
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="conversation-new-message">
                                                            <form>
                                                                <div class="form-group">
                                                                    <textarea class="form-control" rows="2" placeholder="Enter your message..."></textarea>
                                                                </div>

                                                                <div class="clearfix">
                                                                    <button type="submit" class="btn btn-success pull-right">Send message</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer-bar">
            <p id="footer-copyright">
                &copy; 2014 <a href="http://www.adbee.sk/" target="_blank">Adbee digital</a>. Powered by SuperheroAdmin.
            </p>
        </footer>

        <!-- global scripts -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.js"></script>

        <!-- this page specific scripts -->
        <script src="../js/jquery.slimscroll.min.js"></script>

        <!-- theme scripts -->
        <script src="../js/scripts.js"></script>

        <!-- this page specific inline scripts -->
        <script>
            $(document).ready(function () {
                $('.conversation-inner').slimScroll({
                    height: '340px'
                });
            });
        </script>
    </body>
</html>