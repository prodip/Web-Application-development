<?php
include_once '../vendor/autoload.php';

use App\Profiles;
use App\Users;

$userobj = new Users();
$profileobj = new Profiles();

$id = $_GET['id'];
$user_info = $userobj->view_user($id);
$user_profile_info = $profileobj->view_user_details($id);
//echo '<pre>';
//print_r($user_info);
//exit();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SuperheroAdmin - Bootstrap Admin Template</title>

        <!-- bootstrap -->
        <link href="../css/bootstrap/bootstrap.css" rel="stylesheet" />

        <!-- libraries -->
        <link href="../css/libs/font-awesome.css" type="text/css" rel="stylesheet" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="../css/compiled/layout.css">
        <link rel="stylesheet" type="text/css" href="../css/compiled/elements.css">

        <!-- this page specific styles -->
        <link rel="stylesheet" href="../css/libs/datepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/daterangepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/bootstrap-timepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/select2.css" type="text/css" />

        <!-- google font libraries -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>


    </head>
    <body>
    <?php include_once 'admin_header_bar.php';?>
        <div class="container">
            <div class="row">
                <div class="col-md-2" id="nav-col">
                   <?php include_once 'admin_left_sidebar.php';?>
                </div>
                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">

                            <h1>
                                <?php
                                if ($user_info['is_admin'] == 1) {
                                    echo 'Admin';
                                } else {
                                    echo 'User';
                                }
                                ?> Profile
                            </h1>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="main-box">
                                        <h2>Edit Profile</h2>
                                        <form action="profile_store.php" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="firstname">First Name</label>
                                                <input type="text" name="firstname" class="form-control"
                                                       value="<?php if(isset($user_profile_info['firstname'])){
                                                           echo $user_profile_info['firstname'];
                                                       }else{
                                                           echo '';
                                                       }?>"
                                                       id="firstname" placeholder="First Name">
                                                <input type="hidden" name="user_id" value="<?php echo $user_info['id'];?>" class="form-control" id="user_id">
                                            </div>
                                            <div class="form-group">
                                                <label for="lastname">Last Name</label>
                                                <input type="text" name="lastname"
                                                       value="<?php if(isset($user_profile_info['lastname'])){
                                                           echo $user_profile_info['lastname'];
                                                       }else{
                                                           echo '';
                                                       }?>"
                                                       class="form-control" id="lastname" placeholder="Last Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email Address</label>
                                                <input type="email" name="email" class="form-control"
                                                       value="<?php if(isset($user_profile_info['email'])){
                                                           echo $user_profile_info['email'];
                                                       }else{
                                                           echo $user_info['email'];
                                                       }?>"
                                                       id="email" placeholder="Email Address">
                                            </div>
                                            <div class="form-group">
                                                <label for="gender">Gender</label><br>
                                                <input type="radio" name="gender" value="Male" <?php if($user_profile_info['gender'] == 'Male'){echo 'checked';}?> id="gender"> Male  
                                                <input type="radio" name="gender" value="Female" <?php if($user_profile_info['gender'] == 'Female'){echo 'checked';}?> id="gender"> Female
                                            </div>
                                            <div class="form-group">
                                                <label for="mobile_no">Mobile Number</label>
                                                <input type="text" name="mobile_no"
                                                       value="<?php if(isset($user_profile_info['mobile_no'])){
                                                           echo $user_profile_info['mobile_no'];
                                                       }else{
                                                           echo '';
                                                       }?>"
                                                       class="form-control" id="mobile_no" placeholder="Mobile Number">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <textarea name="address" class="form-control" id="address" rows="3">
                                                    <?php if(isset($user_profile_info['address'])){
                                                           echo $user_profile_info['address'];
                                                       }else{
                                                           echo '';
                                                       }?>
                                                </textarea>
                                            </div>

                                           
                                            <div class="form-group form-group-select2">
                                                <label>Select Country</label>
                                                <select name="country" style="width:300px" id="sel2">
                                                    <option value="United States" <?php if($user_profile_info['country'] == 'United States'){echo 'selected';}?>>United States</option> 
                                                    <option value="United Kingdom" <?php if($user_profile_info['country'] == 'United Kingdom'){echo 'selected';}?>>United Kingdom</option> 
                                                    <option value="Afghanistan" <?php if($user_profile_info['country'] == 'Afghanistan'){echo 'selected';}?>>Afghanistan</option> 
                                                    <option value="Albania" <?php if($user_profile_info['country'] == 'Albania'){echo 'selected';}?>>Albania</option> 
                                                    <option value="Algeria" <?php if($user_profile_info['country'] == 'Algeria'){echo 'selected';}?>>Algeria</option> 
                                                    <option value="American Samoa" <?php if($user_profile_info['country'] == 'American Samoa'){echo 'selected';}?>>American Samoa</option> 
                                                    <option value="Andorra" <?php if($user_profile_info['country'] == 'Andorra'){echo 'selected';}?>>Andorra</option> 
                                                    <option value="Bangladesh" <?php if($user_profile_info['country'] == 'Bangladesh'){echo 'selected';}?>>Bangladesh</option> 
                                                    
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="image">Profile Picture</label>
                                                <input type="file" name="image" class="form-control" id="image">
                                                <?php if(isset($user_profile_info['image'])){?>
                                                    <img src="<?php  echo '../img/profile_pics/'.$user_profile_info['image']; ?>" alt="" width="200" height="200">
                                              <?php  }?>
                                                
                                            </div>
                                            <input type="submit" value="Save">
                                            <input type="reset"  value="Reset">
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                   
                                </div>	
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer-bar">
            <p id="footer-copyright">
                &copy; 2014 <a href="http://www.adbee.sk/" target="_blank">Adbee digital</a>. Powered by SuperheroAdmin.
            </p>
        </footer>

        <!-- global scripts -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.js"></script>

        <!-- this page specific scripts -->
        <script src="../js/jquery.maskedinput.min.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script src="../js/moment.min.js"></script>
        <script src="../js/daterangepicker.js"></script>
        <script src="../js/bootstrap-timepicker.min.js"></script>
        <script src="../js/select2.min.js"></script>
        <script src="../js/hogan.js"></script>
        <script src="../js/typeahead.min.js"></script>
        <script src="../js/jquery.pwstrength.js"></script>

        <!-- theme scripts -->
        <script src="../js/scripts.js"></script>

        <!-- this page specific inline scripts -->
        <script>
            $(function ($) {
                //tooltip init
                $('#exampleTooltip').tooltip();

                //nice select boxes
                $('#sel2').select2();

                $('#sel2Multi').select2({
                    placeholder: 'Select a Country',
                    allowClear: true
                });

                //masked inputs
                $("#maskedDate").mask("99/99/9999");
                $("#maskedPhone").mask("(999) 999-9999");
                $("#maskedPhoneExt").mask("(999) 999-9999? x99999");
                $("#maskedTax").mask("99-9999999");
                $("#maskedSsn").mask("999-99-9999");

                $("#maskedProductKey").mask("a*-999-a999", {placeholder: " ", completed: function () {
                        alert("You typed the following: " + this.val());
                    }});

                $.mask.definitions['~'] = '[+-]';
                $("#maskedEye").mask("~9.99 ~9.99 999");

                //datepicker
                $('#datepickerDate').datepicker({
                    format: 'mm-dd-yyyy'
                });

                $('#datepickerDateComponent').datepicker();

                //daterange picker
                $('#datepickerDateRange').daterangepicker();

                //timepicker
                $('#timepicker').timepicker({
                    minuteStep: 5,
                    showSeconds: true,
                    showMeridian: false,
                    disableFocus: false,
                    showWidget: true
                }).focus(function () {
                    $(this).next().trigger('click');
                });

                //autocomplete simple
                $('#exampleAutocompleteSimple').typeahead({
                    prefetch: '/data/countries.json',
                    limit: 10
                });

                //autocomplete with templating
                $('#exampleAutocomplete').typeahead({
                    name: 'twitter-oss',
                    prefetch: '/data/repos.json',
                    template: [
                        '<p class="repo-language">{{language}}</p>',
                        '<p class="repo-name">{{name}}</p>',
                        '<p class="repo-description">{{description}}</p>'
                    ].join(''),
                    engine: Hogan
                });

                //password strength meter
                $('#examplePwdMeter').pwstrength({
                    label: '.pwdstrength-label'
                });

            });
        </script>
    </body>
</html>