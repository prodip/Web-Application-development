<?php
include_once '../vendor/autoload.php';
if(!isset($_SESSION)){
   session_start(); 
}

use App\Users;

$userobj = new Users();
$user_id = $_SESSION['user_id'];
$user_info = $userobj->view_user($user_id);
//echo '<pre>';
//print_r($user_info);
//exit();
?>
<section id="col-left">
    <div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">	
        <ul class="nav nav-pills nav-stacked">
            <li>
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                    <i class="fa fa-chevron-circle-down drop-icon"></i>
                </a>
                <ul class="submenu">
                    <?php if ($user_info['is_admin'] == 1) { ?>
                        <li>
                            <a href="users.php">
                                User list
                            </a>
                        </li>
                    <?php } ?>
                    <li>
                        <a href="user-profile.php">
                            User profile
                        </a>
                    </li>
                </ul>
            </li>
             <li>
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Menus</span>
                    <i class="fa fa-chevron-circle-down drop-icon"></i>
                </a>
                <ul class="submenu">
                        <li>
                            <a href="menu_add.php">
                                Add Menu
                            </a>
                        </li>
                    <li>
                        <a href="menus_manage.php">
                            Manage Menus
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Categories</span>
                    <i class="fa fa-chevron-circle-down drop-icon"></i>
                </a>
                <ul class="submenu">
                        <li>
                            <a href="category_add.php">
                                Add Category
                            </a>
                        </li>
                    <li>
                        <a href="category_manage.php">
                            Manage Categories
                        </a>
                    </li>
                </ul>
            </li>
           
            <li>
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-users"></i>
                    <span>Articles</span>
                    <i class="fa fa-chevron-circle-down drop-icon"></i>
                </a>
                <ul class="submenu">
                        <li>
                            <a href="add_article.php">
                                Add Article
                            </a>
                        </li>
                    <li>
                        <a href="manage_article.php">
                            Manage Article
                        </a>
                    </li>
                </ul>
            </li>
            <?php if ($user_info['is_admin'] == 1) { ?>
                <li>
                    <a href="">
                        <i class="fa fa-money"></i>
                        <span>Pricing</span>
                    </a>
                </li>
            <?php } ?>

            <li>
                <a href="">
                    <i class="fa fa-calendar"></i>
                    <span>Calendar</span>
                </a>
            </li>

            <?php if ($user_info['is_admin'] == 1) { ?>
                <li>
                    <a href="#">
                        <i class="fa fa-picture-o"></i>
                        <span>Gallery</span>
                    </a>
                </li>
            <?php } ?>



        </ul>
    </div>
</section>