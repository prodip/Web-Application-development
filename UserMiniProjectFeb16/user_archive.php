<?php
session_start();
include_once 'vendor/autoload.php';

use App\Articles;

$articleobj = new Articles();
$all_articles_info = $articleobj->select_all_articles();
$user_id = $_GET['user_id'];
$user_articles = $articleobj->select_all_articles_by_user_id($user_id);


use App\Categories;

$categoryobj = new Categories();
$categories_info = $categoryobj->select_published_categories();
//echo '<pre>';
//print_r($user_articles);
//exit();

use App\Menus;

$menuobj = new Menus();

$all_menus = $menuobj->select_all_menus();

$base_url = 'http://localhost/113264/UserMiniProjectFeb16/';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Moderna - Bootstrap 3 flat corporate template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <!-- css -->
        <link href="css/bootstrap/bootstrap.css" rel="stylesheet" />
        <link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
        <link href="css/jcarousel.css" rel="stylesheet" />
        <link href="css/flexslider.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />


        <!-- Theme skin -->
        <link href="skins/default.css" rel="stylesheet" />


    </head>
    <body>
        <div id="wrapper">
            <!-- start header -->
            <header>
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><span>M</span>oderna</a>
                        </div>
                        <div class="navbar-collapse collapse ">
                            <ul class="nav navbar-nav">

                                <?php
                                foreach ($all_menus as $v_menu) {
                                    if ($v_menu['sub_id'] == 1) {
                                        ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"><?php echo $v_menu['title']; ?> <b class=" icon-angle-down"></b></a>
                                            <ul class="dropdown-menu">
                                                <?php 
                                                    foreach($all_menus as $s_menu){
                                                        if($s_menu['parent_id'] == $v_menu['id'] ){
                                                ?>
                                                <li><a href="<?php echo $s_menu['url'].'.php';?>"><?php echo $s_menu['title']; ?></a></li>
                                                        <?php }}?>
                                            </ul>
                                        </li>
                                        
                                    <?php } elseif ($v_menu['sub_id'] == 0) { ?>
                                        
                                        <li><a href="<?php echo $v_menu['url'].'.php';?>"><?php echo $v_menu['title']; ?></a></li>
                                        
                                        <?php
                                    }
                                }
                                ?>
                               
                                <?php if (isset($_SESSION['user_id'])) { ?>
                                    <li><a href="backend/dashboard.php" target="blank">Dashboard</a></li>
                                    <li><a href="logout.php">Logout</a></li>
                                <?php } else { ?>
                                    <li><a href="login.php">Login</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
           
   
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">

                            <?php foreach ($user_articles as $v_user_articles) { ?>
                                <div class="row">
                                    <h2><?php echo $v_user_articles['title'] ?></h2>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <img src="<?php echo 'img/article_feature_images/' . $v_user_articles['image_name']; ?>" width="100%" alt="">
                                        </div>
                                        <div class="col-lg-9">
                                            <p style="text-align: justify">
                                                <?php echo $v_user_articles['summary']; ?>
                                            </p>
                                            <div class="row pull-right">
                                                <a href="article_details.php?id=<?php echo $v_user_articles['article_id']; ?>" class="btn btn-info">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            <?php } ?>
                        </div>
                        <div class="col-lg-4">
                           <aside class="right-sidebar">
				<div class="widget">
					<form class="form-search">
						<input class="form-control" type="text" placeholder="Search..">
					</form>
				</div>
				<div class="widget">
					<h5 class="widgetheading">Categories</h5>
					<ul class="cat">
                                             <?php
                                                    foreach ($categories_info as $v_category) {
                                                        ?>
                                            <li><i class="icon-angle-right"></i><a href="#"><?php echo $v_category['title']?></a><span> (<?php echo count($v_category);?>)</span></li>
                                            <?php } ?>
						
					</ul>
				</div>
				</aside>
                        </div>
                    </div>
                </div>
            </section>
            <section id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h4>Fully responsive</h4>
                                            <div class="icon">
                                                <i class="fa fa-desktop fa-3x"></i>
                                            </div>
                                            <p>
                                                Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h4>Modern Style</h4>
                                            <div class="icon">
                                                <i class="fa fa-pagelines fa-3x"></i>
                                            </div>
                                            <p>
                                                Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h4>Customizable</h4>
                                            <div class="icon">
                                                <i class="fa fa-edit fa-3x"></i>
                                            </div>
                                            <p>
                                                Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h4>Valid HTML5</h4>
                                            <div class="icon">
                                                <i class="fa fa-code fa-3x"></i>
                                            </div>
                                            <p>
                                                Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- divider -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="solidline">
                            </div>
                        </div>
                    </div>
                    <!-- end divider -->
                    <!-- Portfolio Projects -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="heading">Recent Works</h4>
                            <div class="row">
                                <section id="projects">
                                    <ul id="thumbs" class="portfolio">
                                        <!-- Item Project and Filter Name -->
                                        <li class="col-lg-3 design" data-id="id-0" data-type="web">
                                            <div class="item-thumbs">
                                                <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                                <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 1" href="img/works/1.jpg">
                                                    <span class="overlay-img"></span>
                                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                                </a>
                                                <!-- Thumb Image and Description -->
                                                <img src="img/works/1.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                            </div>
                                        </li>
                                        <!-- End Item Project -->
                                        <!-- Item Project and Filter Name -->
                                        <li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 2" href="img/works/2.jpg">
                                                <span class="overlay-img"></span>
                                                <span class="overlay-img-thumb font-icon-plus"></span>
                                            </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/2.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                        </li>
                                        <!-- End Item Project -->
                                        <!-- Item Project and Filter Name -->
                                        <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 3" href="img/works/3.jpg">
                                                <span class="overlay-img"></span>
                                                <span class="overlay-img-thumb font-icon-plus"></span>
                                            </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/3.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                        </li>
                                        <!-- End Item Project -->
                                        <!-- Item Project and Filter Name -->
                                        <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 4" href="img/works/4.jpg">
                                                <span class="overlay-img"></span>
                                                <span class="overlay-img-thumb font-icon-plus"></span>
                                            </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/4.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                        </li>
                                        <!-- End Item Project -->
                                    </ul>
                                </section>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="widget">
                                <h5 class="widgetheading">Get in touch with us</h5>
                                <address>
                                    <strong>Moderna company Inc</strong><br>
                                    Modernbuilding suite V124, AB 01<br>
                                    Someplace 16425 Earth </address>
                                <p>
                                    <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                                    <i class="icon-envelope-alt"></i> email@domainname.com
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="widget">
                                <h5 class="widgetheading">Pages</h5>
                                <ul class="link-list">
                                    <li><a href="#">Press release</a></li>
                                    <li><a href="#">Terms and conditions</a></li>
                                    <li><a href="#">Privacy policy</a></li>
                                    <li><a href="#">Career center</a></li>
                                    <li><a href="#">Contact us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="widget">
                                <h5 class="widgetheading">Latest posts</h5>
                                <ul class="link-list">
                                    <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                                    <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                                    <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="widget">
                                <h5 class="widgetheading">Flickr photostream</h5>
                                <div class="flickr_badge">
                                    <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="copyright">
                                    <p>
                                        <span>&copy; Moderna 2014 All right reserved. </span> <a href="http://bootstraptaste.com/">Bootstrap Themes</a> by BootstrapTaste
                                    </p>
                                    <!-- 
                                        All links in the footer should remain intact. 
                                        Licenseing information is available at: http://bootstraptaste.com/license/
                                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Moderna
                                    -->
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <ul class="social-network">
                                    <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
        <!-- javascript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.fancybox-media.js"></script>
        <script src="js/google-code-prettify/prettify.js"></script>
        <script src="js/portfolio/jquery.quicksand.js"></script>
        <script src="js/portfolio/setting.js"></script>
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/animate.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>
