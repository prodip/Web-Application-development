

<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Book\BookClass_File;
use App\Bitm\SEIP_113264\Utility\utility;

$bookobj = new  BookClass_File();
$Alldata = $bookobj->show();

//var_dump($Alldata);
?>
<html>
    <head>
        <title>
            Book | Home Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New Book</a> |
        <a href="trashed.php">Deleted Books</a>
        <?php
//        print_r($_SESSION);
//        exit();
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
            echo $_SESSION['message'];
            echo utility::message();
        }
            if(isset($_SESSION['mgs'])){
                echo $_SESSION['mgs'];
                unset($_SESSION['mgs']);
            }
        ?>
        <table border='1'>
            <th>SI</th>
            <th>ID</th>
            <th>Title</th>
            <th>Author Name</th>
            <th>Action</th>
            <?php
                $serial = 0;
                foreach($Alldata as $v_book){
                    $serial++;
//                    var_dump($v_book);
//                    exit();
            ?>
            <tr>
                <td><?php echo $serial;?></td>
                <td><?php echo $v_book["id"];?></td>
                <td><?php echo $v_book["title"];?></td>
                <td><?php echo $v_book["author_name"];?></td>
                <td>
                    <a href="show.php?id=<?php echo $v_book["id"];?>">View</a> | 
                    <a href="edit.php?id=<?php echo $v_book["id"];?>">Edit</a> | 
            <!--    <a href="delete.php?id=<?php echo $v_book["id"];?>">Delete</a> -->
                    <a href="trash.php?id=<?php echo $v_book["id"];?>">Delete</a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </body>
</html>



