<?php

 include_once '../../../../vendor/autoload.php';
 
 use App\Bitm\SEIP_113264\ProfilePicture\profilePicture;
 
 $profilepicobj = new profilePicture();
// $mobile_info = $profilepicobj->view();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Home Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="trashed.php">Deleted Items</a> 
       
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Mobile Title</th>
              <th>Mobile Model</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($mobile_info as $v_mobile){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_mobile['id'];?></td>
                    <td><?php echo $v_mobile['title'];?></td>
                    <td><?php echo $v_mobile['model'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_mobile['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $v_mobile['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $v_mobile['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        
        
        
    </body>
</html>
