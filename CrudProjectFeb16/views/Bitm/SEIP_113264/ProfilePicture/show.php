<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Mobile\mobileClass;

$mobileobj = new mobileClass();
$id = $_GET['id'];
$single_view = $mobileobj->show($id);
//echo "<pre>";
//print_r($single_view);
//exit();
 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Details Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
       
        <table border="1">
            <thead>
              <th>ID</th>
              <th>Mobile Title</th>
              <th>Mobile Model</th>
              <th>Picture</th>
              <th>Action</th>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $single_view['id'];?></td>
                    <td><?php echo $single_view['title'];?></td>
                    <td><?php echo $single_view['model'];?></td>
                    <td><img src="show.php?id=<?php echo $single_view['id'];?>" alt="" title="Image"/></td>
                    <td>
                        <a href="edit.php?id=<?php echo $single_view['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $single_view['id'];?>">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        
        
    </body>
</html>

