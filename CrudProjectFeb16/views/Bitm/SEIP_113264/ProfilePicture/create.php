<?php
session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Profile Picture | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="store.php" method="POST" enctype="multipart/form-data">
            <fieldset>
                <legend>Add Your Profile Picture info</legend>
                <label>Enter Your Name:</label>
                <input type="text" name="name">
                <span>
                    <?php 
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
               
                <label>Choose A Image:</label>
                <input type="file" name="profile_pic">
                <span>
                    <?php 
                        if(isset($_SESSION['imagerr'])){
                            echo $_SESSION['imagerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>
