<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Hobby\hobby;

$hobbyobj = new hobby();

$id = $_GET['id'];
$single_view = $hobbyobj->view($id);

$exp = explode(',',$single_view['hobby']);
$single_view['hobby'] = $exp;
//print_r($single_view['name']);
//die();


?>
<html>
    <head>
        <title>
            Hobby | Edit Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="update.php" method="POST" >
            <fieldset>
                <legend>Add Hobby Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $single_view['name'];?>">
                <input type="hidden" name="id" value="<?php echo $single_view['id'];?>">
                <br>
                <label>Select Hobbies:</label>
                <input type="checkbox" name="hobby[]" value="Cricket" <?php if(in_array('Cricket',$single_view['hobby'])){echo 'checked';}?>>Cricket
                <input type="checkbox" name="hobby[]" value="Football" <?php if(in_array('Football',$single_view['hobby'])){echo 'checked';}?>>Football
                <input type="checkbox" name="hobby[]" value="Reading" <?php if(in_array('Reading',$single_view['hobby'])){echo 'checked';}?>>Reading
                <input type="checkbox" name="hobby[]" value="Gardening" <?php if(in_array('Gardening',$single_view['hobby'])){echo 'checked';}?>>Gardening
               
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>