<?php
session_start();
//print_r($_SESSION);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Hobby | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="store.php" method="POST" >
            <fieldset>
                <legend>Add Hobby Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name">
                <span>
                    <?php
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            unset($_SESSION['namerr']);
                        }
                    ?>
                </span>
                <br>
                <label>Select Hobbies:</label>
                <input type="checkbox" name="hobby[]" value="Cricket">Cricket
                <input type="checkbox" name="hobby[]" value="Football">Football
                <input type="checkbox" name="hobby[]" value="Reading">Reading
                <input type="checkbox" name="hobby[]" value="Gardening">Gardening
                <span>
                    <?php
                        if(isset($_SESSION['hobbyerr'])){
                            echo $_SESSION['hobbyerr'];
                            unset($_SESSION['hobbyerr']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>