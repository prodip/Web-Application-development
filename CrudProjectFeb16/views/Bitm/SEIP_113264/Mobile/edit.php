<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Mobile\mobileClass;

$mobileobj = new mobileClass();
$id = $_GET['id'];
$edit_info = $mobileobj->show($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Edit Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="update.php" method="POST">
            <fieldset>
                <legend>Edit Your Favourite Mobile Model</legend>
                <label>Enter Mobile Name:</label>
                <input type="text" name="title" value="<?php echo $edit_info['title'];?>">
                <input type="hidden" name="id" value="<?php echo $edit_info['id'];?>">
                <br>
                
                <label>Enter Mobile Model:</label>
                <input type="text" name="model" value="<?php echo $edit_info['model'];?>">
                <br>
                
                <label>Choose A Image:</label>
                <input type="file" name="m_image" value="<?php echo $edit_info['m_image'];?>">
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
       
    </body>
</html>