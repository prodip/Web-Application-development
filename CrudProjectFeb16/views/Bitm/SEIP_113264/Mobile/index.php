<?php

 include_once '../../../../vendor/autoload.php';
 
 use App\Bitm\SEIP_113264\Mobile\mobileClass;
 use App\Bitm\SEIP_113264\Utility\utility;
 
 $mobileobj = new mobileClass();
 $mobile_info = $mobileobj->view();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Home Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="trashed.php">Deleted Items</a> 
        <?php
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
              //  echo $_SESSION['message'];
                echo utility::message(NULL);
            }
        
        ?>
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Mobile Title</th>
              <th>Mobile Model</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($mobile_info as $v_mobile){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_mobile['id'];?></td>
                    <td><?php echo $v_mobile['title'];?></td>
                    <td><?php echo $v_mobile['model'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_mobile['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $v_mobile['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $v_mobile['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        
        
        
    </body>
</html>
