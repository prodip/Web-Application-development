<?php
session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="store.php" method="POST" enctype="multipart/form-data">
            <fieldset>
                <legend>Add Your Favourite Mobile Model</legend>
                <label>Enter Mobile Name:</label>
                <input type="text" name="title">
                <span>
                    <?php 
                        if(isset($_SESSION['titlerr'])){
                            echo $_SESSION['titlerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter Mobile Model:</label>
                <input type="text" name="model">
                <span>
                    <?php 
                        if(isset($_SESSION['modelerr'])){
                            echo $_SESSION['modelerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Choose A Image:</label>
                <input type="file" name="m_image">
                <span>
                    <?php 
                        if(isset($_SESSION['imagerr'])){
                            echo $_SESSION['imagerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>
