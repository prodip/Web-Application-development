<?php

 include_once '../../../../vendor/autoload.php';
 
 use App\Bitm\SEIP_113264\Gender\gender;
 use App\Bitm\SEIP_113264\Utility\utility;
 
 $genderobj = new gender();
 $all_info = $genderobj->index();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Gender | Home Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="trashed.php">Deleted Items</a> 
        <?php
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
              //  echo $_SESSION['message'];
                echo utility::message(NULL);
            }
        
        ?>
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($all_info as $v_info){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_info['id'];?></td>
                    <td><?php echo $v_info['name'];?></td>
                    <td><?php echo $v_info['gender'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_info['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $v_info['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $v_info['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        
        
        
    </body>
</html>



