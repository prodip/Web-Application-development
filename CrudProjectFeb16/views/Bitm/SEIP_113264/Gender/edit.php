<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Gender\gender;

$genderobj = new gender();

$id = $_GET['id'];
$singleData = $genderobj->view($id);
//print_r($singleData);
//exit();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Gender | Edit Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="update.php" method="POST" >
            <fieldset>
                <legend>Add Birthday Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $singleData['name'];?>">
                <input type="hidden" name="id" value="<?php echo $singleData['id'];?>">
                <br>
                <label>Select Gender:</label>
                Male: <input type="radio" name="gender" value="Male" <?php if($singleData['gender']=='Male') { echo 'checked'; }?>>
                Female:<input type="radio" name="gender" value="Female" <?php if($singleData['gender']=='Female') { echo 'checked'; }?>>
                <br>
                <input type="checkbox" name="term" value="1" <?php if($singleData['term_and_condition'] ==1){ echo 'checked';};?>> You should obey this term and conditionable
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>
