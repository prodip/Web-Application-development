<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Gender\gender;

$genderobj = new gender();

$id = $_GET['id'];
$singleData = $genderobj->view($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Gender | View Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="index.php">Home</a> 
        
        <table border="1">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Term and Condition</th>
              <th>Action</th>
            </thead>
            <tbody>
               
                <tr>
                    <td><?php echo $singleData['id'];?></td>
                    <td><?php echo $singleData['name'];?></td>
                    <td><?php echo $singleData['gender'];?></td>
                    <td> <input type="checkbox" name="term"  <?php if($singleData['term_and_condition'] ==1){ echo 'checked';};?>></td>
                    <td>
                        <a href="show.php?id=<?php echo $singleData['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $singleData['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $singleData['id'];?>">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        
        
    </body>
</html>
