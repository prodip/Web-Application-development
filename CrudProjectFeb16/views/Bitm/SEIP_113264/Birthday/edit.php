<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Birthday\birthdayClass;

$birthdayobj = new birthdayClass();
$id = $_GET['id'];
$singleData = $birthdayobj->show($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Birthday | Edit Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="update.php" method="POST" >
            <fieldset>
                <legend>Add Birthday Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name" value="<?php echo $singleData['name'];?>">
                <input type="hidden" name="id" value="<?php echo $singleData['id'];?>">
                <br>
                <label>Enter Your Birthday:</label>
                <input type="text" name="birthday" value="<?php echo $singleData['birthday'];?>">
                <br>
                <label>Enter  Birthday Place:</label>
                <input type="text" name="b_place" value="<?php echo $singleData['b_place'];?>">
                <br>
                <label>Enter Email Address:</label>
                <input type="text" name="email_address" value="<?php echo $singleData['email_address'];?>">
                <br>
                <label>Enter Mobile Number:</label>
                <input type="text" name="mobile" value="<?php echo $singleData['mobile'];?>">
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>

