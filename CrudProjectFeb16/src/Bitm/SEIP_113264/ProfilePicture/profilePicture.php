<?php

namespace App\Bitm\SEIP_113264\ProfilePicture;

session_start();

use PDO;

class profilePicture {
    
    public $pdo = '';
    public $table='profilePictures';
    public $name='';
    public $profile_pic='';

    
    
    public function __construct() {
        try {
            $conn = new PDO("mysql:host=localhost;dbname=db_crudprojectfeb16", "root", "");
             $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         //   echo 'ok';
//            echo '<pre>';
//        print_r($conn);
//        exit();
           // return $conn;
        } catch (PDOException $e) {
            echo 'Connection failed! ' . $e->getMessage();
        }
         $this->pdo = $conn;
    }

    public function prepare_data($data = '') {
//        echo '<pre>';
//        print_r($data);
//        exit();
        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->name = $data['name'];
        }
        if(array_key_exists('profile_pic', $data) && !empty($data['profile_pic'])){
            $this->profile_pic = $data['profile_pic'];
        }
    }
    
    public function insert(){
        $sql = "INSERT INTO `$this->table`(`name`, `profile_pic`)VALUES(:name, :profile_pic))";
        
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':profile_pic', $this->profile_pic);
        
//        echo '<pre>';
//        print_r($r);
//        exit();
        
        $stmt->execute();
    }
    
    
    
    
    
    
    

}
