<?php

namespace App\Bitm\SEIP_113264\Actor;
session_start();
use App\Bitm\SEIP_113264\Utility\utility;

class actor{
    
    public $id = '';
    public $name ='';
    public $actors = '';
    
   public function __construct(){
       $conn = mysql_connect('localhost','root','') or die("Unable to connect with Server");
       mysql_select_db('db_crudprojectfeb16', $conn) or die("Unable to select Database");
   }
    
    public function prepare($data = ''){
       // print_r($data);
       // die();
        if(isset($data['id']) && !empty($data['id'])){
        $this->id = $data['id'];
        }
        
        if(isset($data['name']) && !empty($data['name'])){
        $this->name = $data['name'];
        }else{
        //    echo 'Yes'; die();
            $_SESSION['namerr'] = 'Please enter a Name';
            header('location:create.php');
        }
        $actors = $data['actor'];
        $itmes = implode(',', $actors);
        $data['actor']=$itmes;
       // print_r($data['actor']);
       // die();
         if(isset($data['actor']) && !empty($data['actor'])){
        $this->actors = $data['actor'];
        }else{
            $_SESSION['actorerr'] = 'Please Select  Your Favorite Actors';
            header('location:create.php');
        }
    }
    
    public function store(){
        
        if(!empty($this->name) && !empty($this->actors)){
        $query = "INSERT INTO actors(name, actor) VALUES('".$this->name."','".$this->actors."')";
        mysql_query($query);
        
        utility::message('Save Successfully!');
        header('location:index.php');
        
        }else{
             $_SESSION['namerr'] = 'Please enter a Name';
             $_SESSION['actorerr'] = 'Please Select  Your Favorite Actors';
            header('location:create.php');
        }
        
    }
    
    public function index(){
        $data = array();
        $query = "SELECT * FROM actors WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function view($id = ''){
        $query ="SELECT * FROM actors WHERE id=".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
    public function update(){
       // print_r($this->id);
       // die();
        $query = "UPDATE actors SET name='".$this->name."', actor='".$this->actors."' WHERE id=".$this->id;
       // print_r($query);
       // die();
        mysql_query($query);
        
        utility::message("Updated Successfully!");
        header('location:index.php');
    }
    
    public function trash($id = ''){
        $query = "UPDATE actors SET deleted_at='".date('Y-m-d')."' WHERE id=".$id;
//        print_r($query);
//        die();
        mysql_query($query);
        
        utility::message("Deleted Successfully!");
        header('location:index.php');
    }
    
    public function trashed(){
        $data = array();
        $query = "SELECT * FROM actors WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $data[]= $row;
        }
        return $data;
    }
    
    public function restore($id = ''){
        $query = "UPDATE actors SET deleted_at=NULL WHERE id=".$id;
//        print_r($query);
//        die();
        mysql_query($query);
        
        utility::message("Restore Successfully!");
        header('location:index.php');
    }
    
    public function delete($id = ''){
        $query = "DELETE FROM actors WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Permanently Deleted!");
        header('location:trashed.php');
    }
    
    
}

