<?php

$str = <<<Any_Text
        <h1>Example of string spanning multiple lines
        useing heredoc syntax.</h1>
Any_Text;

echo $str .'</br>';


class example{
    var $mul;
    var $add;
    
    function sum(){
        $this->mul = 10;
        $this->add = array(5,8.15,45);
    }
  
}

$obj = new example();

var_dump($obj);

$exm = <<<proDip
        <h1>show :</h1> $obj->mul
        <h1>show:</h1> {$obj->add[1]}
proDip;

echo $exm;

?>
