<?php
namespace App\Bitm\SEIP_113264\PhoneBook;
session_start();

use App\Bitm\SEIP_113264\PhoneBook\utility;

class phoneClass {
    
    public function __construct(){
        $conn = mysql_connect('localhost', 'root', '') or die(mysql_error());
        mysql_select_db('db_phonebook', $conn)or die(mysql_error());
    }
    
    public function store($data = ''){
        $name = $data['name'];
        $email_address = $data['email_address'];
        $mobile = $data['mobile'];
        
        if(!empty($name) && !empty($email_address) && !empty($mobile)){
            $query = "INSERT INTO phonebooks (name, email_address, mobile) VALUES('".$name."', '".$email_address."', '".$mobile."')";
            mysql_query($query);
            
            utility::message("Save Successfully!");
            header('location:index.php');
            
        }else{
            $_SESSION['namerr'] = "Please Enter a Name!";
            $_SESSION['emailerr'] = "Please Enter a Email Address!";
            $_SESSION['mobilerr'] = "Please Enter a Mobile Number!";
            
            header('location:create.php');
        }
      
    }
    
    public function view(){
        $data =array();
        $query = "SELECT * FROM phonebooks WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        
        while($onerecode = mysql_fetch_assoc($result)){
            $data[] = $onerecode;
        }
        return $data;
    }
    
    public function show($id = ''){
        $query = "SELECT * FROM phonebooks WHERE id=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
     public function update($data = ''){
        $id = $data['id'];
        $name = $data['name'];
        $email_address = $data['email_address'];
        $mobile = $data['mobile'];
        
        $query = "UPDATE phonebooks SET name='".$name."', email_address='".$email_address."', mobile='".$mobile."', updated_at='".date('Y-m-d')."' WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Updated Successfully!");
        header('location:index.php');
     }
     
     public function delete($id = ''){
         $query = "DELETE FROM phonebooks WHERE id=".$id;
         mysql_query($query);
         
         utility::message("deleted Permanently!");
         header('location:trashed.php');
     }
     
     public function trash($id = ''){
         $query = "UPDATE phonebooks SET deleted_at = '".date('Y-m-d  H:i:s')."' WHERE id=".$id;
         mysql_query($query);
         
        utility::message("deleted Successfully!");
        header('location:index.php');
     }
    
     public function trashed(){
        $data =array();
        $query = "SELECT * FROM phonebooks WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($onerecode = mysql_fetch_assoc($result)){
            $data[] = $onerecode;
        }
        return $data;
    }
     
    public function restore($id = ''){
       
        $query = "UPDATE phonebooks SET deleted_at=NULL WHERE id=".$id;
        mysql_query($query);
        
        utility::message("Restore Successfully!");
        header('location:index.php');
     }
     
    
}
