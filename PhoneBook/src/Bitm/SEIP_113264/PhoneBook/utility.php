<?php
namespace App\Bitm\SEIP_113264\PhoneBook;
if(!isset($_SESSION)){
    session_start();
}

class utility {
    
    static public function message($message = ''){
        
        if(is_null($message)){
            $smessage = self::getMessage();
            return $smessage;
        }else{
            self::setMessage($message);
        }
        
    }
    static private function getMessage(){
            $_message = $_SESSION['message'];
            $_SESSION['message'] = '';
            return $_message;
        }
    
    static private function setMessage($message){
        $_SESSION['message']= $message;
    }
    
}
