<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\PhoneBook\phoneClass;

$phoneobj = new phoneClass();
$id = $_GET['id'];
$singleData = $phoneobj->show($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            PhoneBook | Details Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a> |
        <a href="create.php">Add New</a>
        <table border="1">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Email Address</th>
              <th>Mobile</th>
              <th>Action</th>
           </thead>
           <tbody>
              
               <tr>
                   <td><?php echo $singleData['id']?></td>
                   <td><?php echo $singleData['name']?></td>
                   <td><?php echo $singleData['email_address']?></td>
                   <td><?php echo $singleData['mobile']?></td>
                   <td>
                       <a href="edit.php?id=<?php echo $singleData['id']?>">Edit</a> |
                       <a href="delete.php?id=<?php echo $singleData['id']?>">Delete</a> 
                   </td>
               </tr>
           </tbody>
    </table>


</body>
</html>

