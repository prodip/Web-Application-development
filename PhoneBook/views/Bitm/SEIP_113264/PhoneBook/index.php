<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\PhoneBook\phoneClass;
use App\Bitm\SEIP_113264\PhoneBook\utility;

$phoneobj = new phoneClass();
$allData = $phoneobj->view();



?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            PhoneBook | Home
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="trashed.php">Deleted Items</a> 
        <?php
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
            echo utility::message(NULL);
        }
        
        ?>
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Email Address</th>
              <th>Mobile</th>
              <th>Action</th>
           </thead>
           <tbody>
               <?php 
               $s = 0;
                foreach($allData as $v_data){
                    $s++;
                
               ?>
               <tr>
                   <td><?php echo $s;?></td>
                   <td><?php echo $v_data['id']?></td>
                   <td><?php echo $v_data['name']?></td>
                   <td><?php echo $v_data['email_address']?></td>
                   <td><?php echo $v_data['mobile']?></td>
                   <td>
                       <a href="show.php?id=<?php echo $v_data['id']?>">View</a> |
                       <a href="edit.php?id=<?php echo $v_data['id']?>">Edit</a> |
                       <a href="trash.php?id=<?php echo $v_data['id']?>">Delete</a> 
                   </td>
               </tr>
               <?php } ?>
           </tbody>
    </table>


</body>
</html>
