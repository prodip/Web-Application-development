<?php
session_start();

?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            PhoneBook | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add Contact in the PhoneBook</legend>
                <label>Enter Name</label>
                <input type="text" name="name" >
                <span>
                    <?php
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            $_SESSION['namerr'] = '';
                        }
                    ?>
                </span>
                <br/>
                <label>Enter Email Address</label>
                <input type="text" name="email_address" >
                <span>
                    <?php
                        if(isset($_SESSION['emailerr'])){
                            echo $_SESSION['emailerr'];
                            $_SESSION['emailerr'] = '';
                        }
                    ?>
                </span>
                <br/>
                <label>Enter Mobile Number</label>
                <input type="text" name="mobile" >
                <span>
                    <?php
                        if(isset($_SESSION['mobilerr'])){
                            echo $_SESSION['mobilerr'];
                            $_SESSION['mobilerr'] = '';
                        }
                    ?>
                </span>
                
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>

</body>
</html>
