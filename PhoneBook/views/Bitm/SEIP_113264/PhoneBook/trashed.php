<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\PhoneBook\phoneClass;
use App\Bitm\SEIP_113264\PhoneBook\utility;

$phoneobj = new phoneClass();
$deletedData = $phoneobj->trashed();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            PhoneBook | Trashed Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="index.php">Home</a> 
        <?php
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
            echo utility::message(NULL);
        }
        
        ?>
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Email Address</th>
              <th>Mobile</th>
              <th>Action</th>
           </thead>
           <tbody>
               <?php 
               $s = 0;
                foreach($deletedData as $v_data){
                    $s++;
                
               ?>
               <tr>
                   <td><?php echo $s;?></td>
                   <td><?php echo $v_data['id']?></td>
                   <td><?php echo $v_data['name']?></td>
                   <td><?php echo $v_data['email_address']?></td>
                   <td><?php echo $v_data['mobile']?></td>
                   <td>
                       <a href="show.php?id=<?php echo $v_data['id']?>">View</a> |
                       <a href="edit.php?id=<?php echo $v_data['id']?>">Edit</a> |
                       <a href="restore.php?id=<?php echo $v_data['id']?>">Restore</a> |
                       <a href="delete.php?id=<?php echo $v_data['id']?>">Delete</a> 
                   </td>
               </tr>
                <?php } ?>
           </tbody>
    </table>


</body>
</html>

