<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\PhoneBook\phoneClass;

$phoneobj = new phoneClass();
$id = $_GET['id'];
$singleData = $phoneobj->show($id);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            PhoneBook | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="update.php" method="POST">
            <fieldset>
                <legend>Add Contact in the PhoneBook</legend>
                <label>Enter Name</label>
                <input type="text" name="name"  value="<?php echo $singleData['name'] ?>">
                <input type="hidden" name="id"  value="<?php echo $singleData['id'] ?>">
                <br/>
                <label>Enter Email Address</label>
                <input type="text" name="email_address" value="<?php echo $singleData['email_address'] ?>" >
                <br/>
                <label>Enter Mobile Number</label>
                <input type="text" name="mobile" value="<?php echo $singleData['mobile'] ?>">
                
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>

</body>
</html>
