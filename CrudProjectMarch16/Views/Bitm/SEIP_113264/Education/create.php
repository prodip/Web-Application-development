<!DOCTYPE html>
<html>
    <head>
        <title>
            Educational Level | Create page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        <h2>Create page</h2><hr>
        <?php
        session_start();
//        var_dump($_SESSION);
//        exit();
        if (isset($_SESSION['mgs'])) {
            echo $_SESSION['mgs'];
            unset($_SESSION['mgs']);
        }
        ?>
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add Your Latest Educational Qualification</legend>
                <table>
                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" name="name">
                        </td>
                        <td>
                            <?php
         
                            if (isset($_SESSION['namerr'])) {
                                echo $_SESSION['namerr'];
                                unset($_SESSION['namerr']);
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Select Level</label>
                        </td>
                        <td>
                            <select name="edu_level">
                                <option>S.S.C</option>
                                <option>H.S.C</option>
                                <option>B.Sc</option>
                                <option>M.Sc</option>
                                <option>other</option>
                            </select>
                            <input type="text" name="edu_institution">
                        </td>
                        <td>
                            <?php
         
                            if (isset($_SESSION['edu_inserr'])) {
                                echo $_SESSION['edu_inserr'];
                                unset($_SESSION['edu_inserr']);
                            }
                            ?>
                        </td>
                        
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Save">
                            <input type="reset" value="Reset">
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </body>
</html>
