<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Education\EducationClass;

$eduobj = new EducationClass(); 

$id = $_GET['id'];
$singleData = $eduobj->ViewSingleData($id);
//echo '<pre>';
//print_r($singleData);
//exit();

?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Educational Level | Single View page
        </title>
    </head>
    <body>
        <h2>Single View page</h2><hr>

        <table border="1">
            <th>ID</th>
            <th>Name</th>
            <th>Institution</th>
            <th>Action</th>
           
            <tr>
                <td>
                   <?php echo $singleData['id'];?>
                </td>
                <td>
                   <?php echo $singleData['name'];?>
                </td>
                <td>
                   <?php echo $singleData['edu_institution'];?>
                </td>
                <td>
                    <a href="edit.php?id=<?php echo $singleData['id'];?>">Edit</a> | 
                    <a href="delete.php?id=<?php echo $singleData['id'];?>">Delete</a> 
                </td>
            </tr>
        </table>
    </body>
</html>



