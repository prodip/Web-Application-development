<h2>User Educational Qualification</h2>
<a href="create.php">Add New</a>

<?php
session_start();
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Education\EducationClass;

$eduobj = new EducationClass();
$Allinfo = $eduobj->show();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Educational Level | Show page
        </title>
    </head>
    <body>
        <h2>Show page</h2><hr>
        <?php
//        var_dump($_SESSION);
//        exit();
        if(isset($_SESSION['mgs'])){
            echo $_SESSION['mgs'];
            unset($_SESSION['mgs']);
        }
        ?>
        <table border="1">
            <th>SI</th>
            <th>ID</th>
            <th>Name</th>
            <th>Institution</th>
            <th>Action</th>
            <?php 
                $serial = 0;
                foreach($Allinfo as $onedata){
                    
                $serial++;
                if(!empty($Allinfo)){
            ?>
            <tr>
                <td>
                   <?php echo $serial;?>
                </td>
                <td>
                   <?php echo $onedata['id'];?>
                </td>
                <td>
                   <?php echo $onedata['name'];?>
                </td>
                <td>
                   <?php echo $onedata['edu_institution'];?>
                </td>
                <td>
                    <a href="show.php?id=<?php echo $onedata['id'];?>">Views</a> | 
                    <a href="edit.php?id=<?php echo $onedata['id'];?>">Edit</a> | 
                    <a href="delete.php?id=<?php echo $onedata['id'];?>">Delete</a> 
                    
                </td>
            </tr>
                <?php }else{ ?>
                    <tr>
                <td>
                   <?php echo "OOPS! There is not in the Table!";?>
                </td>
            </tr>
     <?php           } }?>
        </table>
    </body>
</html>


