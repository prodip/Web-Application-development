<?php

namespace App;
class Users
{

    public $id = '';
    public $unique_id = '';
    public $username = '';
    public $password = '';
    public $email = '';
    public $is_admin = '';

    public $user_id = '';
    public $first_name = '';
    public $last_name = '';
    public $personal_phone = '';
    public $home_phone = '';
    public $office_phone = '';
    public $current_address = '';
    public $permanent_address = '';
    public $profile_pic = '';
    public $modified_by = '';

    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die ('Unable to connect with Database Server.');
        mysql_select_db('usrreg',$conn);
    }

    public function prepare($data = '')
    {
       // print_r($data);
     //   die();
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->unique_id = uniqid();

        if (isset($data['username']) && !empty($data['username'])) {
            $this->username = $data['username'];
        }

        if (isset($data['password']) && !empty($data['password'])) {
            $this->password = $data['password'];
        }

        if (isset($data['email']) && !empty($data['email'])) {
            $this->email = $data['email'];
        }

        if (isset($data['is_admin']) && !empty($data['is_admin'])) {
            $this->is_admin = $data['is_admin'];
        }

        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $this->user_id = $data['user_id'];
        }

        if (isset($data['first_name']) && !empty($data['first_name'])) {
            $this->first_name = $data['first_name'];
        }

        if (isset($data['last_name']) && !empty($data['last_name'])) {
            $this->last_name = $data['last_name'];
        }

        if (isset($data['personal_phone']) && !empty($data['personal_phone'])) {
            $this->personal_phone = $data['personal_phone'];
        }

        if (isset($data['profile_pic']) && !empty($data['profile_pic'])) {
            $this->profile_pic = $data['profile_pic'];
        }

        if (isset($data['modified_by']) && !empty($data['modified_by'])) {
            $this->modified_by = $data['modified_by'];
        }


    }

    public function register()
    {
      //  print_r($this->username);
   //  print_r($this->password);
   //  print_r($this->unique_id);
   //    print_r($this->email);
     //   die();
        
        if (isset($this->username) && !empty($this->password) && !empty($this->email)) {
            $query = "INSERT INTO `usrreg`.`users` (`unique_id`, `username`, `password`, `email`) VALUES ( '".$this->unique_id."', '".$this->username."', '".$this->password."', '".$this->email."')";
            mysql_query($query);
            if (mysql_query($query)) {
                $_SESSION['Message'] = "Registration pending. Wait for approval.";
            }
        } else{
            $_SESSION['Message'] = "Fill up all field carefully.";
        }
        header('location:../index.php');
    }

    public function login()
    {
       session_start();
//        print_r($data['username']);
//        print_r($data['password']);
//        die();
        $query = "SELECT * FROM `users` WHERE (`username`= '" . $this->username . "' OR `email`= '" . $this->username . "') AND `is_active`= '1'";
        $run_query = mysql_query($query);

//        echo $query;
//        die();

        $result = mysql_fetch_assoc($run_query);
       // print_r($result);
      //  die();
      $id = $result['id'];
    $username = $result['username'];
     $useremail = $result['email'];
   $password = $result['password'];
    //   die();
//        $usertype = $result['type'];

//        echo $userid."  ".$password;
//        die();
   
  // echo $this->password . $password;
  // echo $this->username .$useremail;
  // die();

        if (( $this->password == $password) && ($this->username == $useremail)) {
            $_SESSION['Message'] = "success";
            $_SESSION['user_id'] = $id;
            header('location:../index.php');
        } elseif ($this->username == $username) {
            $_SESSION['Message'] = "Incorrect Password.";
           header('location:login.php');
        } elseif($this->password == $password) {
            $_SESSION['Message'] = "Incorrect Username.";
            header('location:login.php');
        } else {
            $_SESSION['Message'] = "Incorrect information.";
           header('location:../index.php');
        }

    }

    public function profiles()
    {
        session_start();
        $user_id = $_SESSION['Message'];
        $allData = array();
        $query = "SELECT * FROM `profiles` WHERE user_id =".$user_id;
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)){
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function profile($id = '')
    {
        session_start();
        $user_id = $_SESSION['Message'];
        $query = "SELECT * FROM `profiles` WHERE (user_id ='".$this->user_id."' AND id = '".$id."')";
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function create_profile($id = '')
    {
        session_start();
        $user_id = $_SESSION['Message'];
        $query = "INSERT INTO `userreg`.`profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `current_address`, `permanent_address`, `profile_pic` `modified_by`)"
        ."VALUES (NULL, '".$user_id."', '".$this->first_name."', '".$this->last_name."', '".$this->personal_phone."', '".$this->home_phone."', '".$this->office_phone."', '".$this->current_address."', '".$this->permanent_address."', '".$this->profile_pic."', '".$user_id."');";
        $run_query = mysql_query($query);
        if (mysql_fetch_assoc($run_query))
        {
            $_SESSION['Message'] = "Profile successfully updated.";
        }
    }

    public function edit_profile($id = '')
    {
        session_start();
        $user_id = $_SESSION['Message'];
        $query = "UPDATE `userreg`.`profiles` SET `first_name` = '".$this->first_name."', `last_name` = '".$this->last_name."', `personal_phone` = '".$this->personal_phone."', `home_phone` = '".$this->home_phone."', `office_phone` = '".$this->office_phone."', `current_address` = '".$this->current_address."', `permanent_address` = '".$this->permanent_address."', `profile_pic` = '".$this->profile_pic."', `modified_by` = '".$user_id."' WHERE `profiles`.`id` = ".$id;
        $run_query = mysql_query($query);
        if (mysql_fetch_assoc($run_query))
        {
            $_SESSION['Message'] = "Profile successfully updated.";
        }
    }



}