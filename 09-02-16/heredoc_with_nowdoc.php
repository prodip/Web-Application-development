<?php

$str = <<<Any_Text
        <h1>Example of string spanning multiple lines
        useing heredoc syntax.</h1>
Any_Text;

echo $str .'</br>';


$str1 = <<<'Any_Text'
        <h1>Example of string spanning multiple lines
        useing nowdoc syntax.</h1>
Any_Text;

echo $str1 .'</br>';

