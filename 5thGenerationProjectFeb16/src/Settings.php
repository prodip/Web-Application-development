<?php


namespace App;
use PDO;

class Settings
{
    public $title = '';
    public $subtitle = '';
    public $system_emil = '';
    public $footer = '';
    public $reg_permission = '';
    public $user_activity = '';
    public $sqr_logo = '';
    public $wide_logo = '';
    public $table = 'settings';

    public function __construct(){
        try{
            $this->pdo = new PDO('mysql:host=localhost;dbname=db_owncms', 'root', '');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo "connection failed!" . $e->getMessage();
        }
    }

    public function prepare($data = ''){
        echo '<pre>';
        print_r($data);
        exit();
        if (isset($data['title']) && !empty($data['title'])){
            $this->title = $data['title'];
        }
        if (isset($data['subtitle']) && !empty($data['subtitle'])){
            $this->subtitle = $data['subtitle'];
        }
        if (isset($data['system_emil']) && !empty($data['system_emil'])){
            $this->system_emil = $data['system_emil'];
        }
        if (isset($data['footer']) && !empty($data['footer'])){
            $this->footer = $data['footer'];
        }
        if (isset($data['reg_permission']) && !empty($data['reg_permission'])){
            $this->reg_permission = $data['reg_permission'];
        }

        if (isset($data['user_activity']) && !empty($data['user_activity'])){
            $this->user_activity = $data['user_activity'];
        }

        if (isset($data['sqr_logo']) && !empty($data['sqr_logo'])){
            $this->sqr_logo = $data['sqr_logo'];
        }
        if (isset($data['wide_logo']) && !empty($data['wide_logo'])){
            $this->wide_logo = $data['wide_logo'];
        }

    }

    public function insert(){
        $sql = "INSERT INTO $this->table (title, subtitle, email, reg_permission, user_active, footer, sqr_logo, wide_logo) VALUES (:title, :subtitle, :email, :reg_permission, :user_active, :footer, :sqr_logo, :wide_logo)";
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':title', $this->title);
        $q->bindParam(':subtitles', $this->subtitle);
        $q->bindParam(':email', $this->system_emil);
        $q->bindParam(':registration', $this->reg_permission);
        $q->execute();
        header('location:setting.php');
    }

    public function update(){
        $sql = "UPDATE $this->table SET title = :title, subtitle = :subtitles, email = :email, reg_permission = :registration, user_active = :active, footer = :footer, sqr_logo = :sqr_logo, wide_logo = :wide_logo";
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':title', $this->title);
        $q->bindParam(':subtitles', $this->subtitle);
        $q->bindParam(':email', $this->system_emil);
        $q->bindParam(':registration', $this->reg_permission);
        $q->bindParam(':active', $this->user_activity);
        $q->bindParam(':footer', $this->footer);
        $q->bindParam(':sqr_logo', $this->sqr_logo);
        $q->bindParam(':wide_logo', $this->wide_logo);
        $q->exicute();
        header('location:settings.php');
    }

    public function show_settings(){
        $sql = "SELECT * FROM $this->table";
        $q = $this->pdo->prepare($sql);
        $q->execute();
        $result = $q->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}