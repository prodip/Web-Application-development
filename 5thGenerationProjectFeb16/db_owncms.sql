-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2016 at 07:47 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_owncms`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `html_summary` text NOT NULL,
  `details` text NOT NULL,
  `html_details` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleled_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `title`, `sub_title`, `summary`, `html_summary`, `details`, `html_details`, `publication_status`, `created_at`, `modified_at`, `deleled_at`) VALUES
(2, 4, 'Curabitur aliquet quam id dui posuere blandit edit sss', 'Donec sollicitudin molestie malesuada editeesss', 'Edit sss Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vivamus suscipit tortor eget felis porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.\r\n', '&lt;h1&gt;&lt;span class=&quot;marker&quot;&gt;Edit sss Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.&lt;/span&gt; Donec&lt;span style=&quot;font-size:20px&quot;&gt; rutrum congue leo eget malesuada. Vestibulum ac diam sit&lt;/span&gt; amet quam vehicula elementum sed sit amet dui. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Curabitur arcu erat, &lt;em&gt;accumsan id imperdiet et, porttitor at sem. Vivamus suscipit&lt;/em&gt; tortor eget felis porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.&lt;/h1&gt;\r\n', 'Edit sss Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Pellentesque in ipsum id orci porta dapibus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.\r\n', '&lt;h2&gt;&lt;strong&gt;&lt;span class=&quot;marker&quot;&gt;Edit sss&lt;/span&gt; Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie males&lt;/strong&gt;uada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, &lt;span style=&quot;font-size:20px&quot;&gt;accumsan id imperdiet et, porttitor at sem&lt;/span&gt;. Pellentesque in ipsum id orci porta dapibus. &lt;span class=&quot;marker&quot;&gt;Donec sollicitudin molesti&lt;/span&gt;e malesuada. Pellentesque in ipsum id orci porta dapibus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.&lt;/h2&gt;\r\n', 0, '2016-04-20 12:07:13', '0000-00-00 00:00:00', NULL),
(3, 4, 'eeeeeCurabitur aliquet quam id dui posuere blandit edit', 'eeeeeDonec sollicitudin molestie malesuada edit', 'eeeeeVivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec Edit rutrum congue leo eget malesuada. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vivamus suscipit tortor eget felis porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.\r\n', '&lt;p&gt;&lt;span class=&quot;marker&quot;&gt;eeeeeVivamus magna justo, lacinia eget consectetur sed, convallis at tellus.&lt;/span&gt; Donec&lt;span style=&quot;font-size:20px&quot;&gt; &lt;/span&gt;&lt;strong&gt;&lt;span class=&quot;marker&quot;&gt;Edit&lt;/span&gt; &lt;/strong&gt;&lt;span style=&quot;font-size:20px&quot;&gt;rutrum congue leo eget malesuada. Vestibulum ac diam sit&lt;/span&gt; amet quam vehicula elementum sed sit amet dui. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Curabitur arcu erat, &lt;em&gt;accumsan id imperdiet et, porttitor at sem. Vivamus suscipit&lt;/em&gt; tortor eget felis porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla quis lorem ut libero malesuada feugiat.&lt;/p&gt;\r\n', 'eeeeeEdit Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus. Donec sollicitudin molestie malesuada. Pellentesque in ipsum id orci porta dapibus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.\r\n', '&lt;h2&gt;&lt;strong&gt;&lt;span class=&quot;marker&quot;&gt;eeeeeEdit&lt;/span&gt; Curabitur aliquet quam id dui posuere blandit. Donec sollicitudin molestie males&lt;/strong&gt;uada. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, &lt;span style=&quot;font-size:20px&quot;&gt;accumsan id imperdiet et, porttitor at sem&lt;/span&gt;. Pellentesque in ipsum id orci porta dapibus. &lt;span class=&quot;marker&quot;&gt;Donec sollicitudin molesti&lt;/span&gt;e malesuada. Pellentesque in ipsum id orci porta dapibus. Quisque velit nisi, pretium ut lacinia in, elementum id enim.&lt;/h2&gt;\r\n', 1, '2016-04-20 19:34:32', '0000-00-00 00:00:00', NULL),
(4, 4, 'Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. ', 'Sed porttitor lectus nibh.', 'Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Curabitur aliquet quam id dui posuere blandit. Cras ultricies ligula sed magna dictum porta. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.\r\n', '&lt;p&gt;&lt;strong&gt;Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auct&lt;/strong&gt;or sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.&lt;u&gt; Curabitur aliquet quam id dui posuere blandit. Cras ultricies ligula sed magna dictum porta. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget ma&lt;/u&gt;lesuada. Nulla quis lorem ut libero malesuada feugiat. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.&lt;/p&gt;\r\n', 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla porttitor accumsan tincidunt.\r\n', '&lt;p&gt;&lt;em&gt;Quisque velit nisi, pretium ut lacinia in, eleme&lt;/em&gt;ntum id enim. &lt;img alt=&quot;heart&quot; src=&quot;http://localhost/5thGenerationProjectFeb16/third%20party/ckeditor/plugins/smiley/images/heart.png&quot; style=&quot;height:23px; width:23px&quot; title=&quot;heart&quot; /&gt;Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel&lt;span class=&quot;marker&quot;&gt;, ullamcorper sit amet ligula. Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vestibulum ac diam sit amet quam vehicula elem&lt;/span&gt;entum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla porttitor accumsan tincidunt.&lt;/p&gt;\r\n', 1, '2016-04-24 21:48:40', '0000-00-00 00:00:00', NULL),
(5, 4, 'Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. ', 'Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. ', 'Nulla quis lorem ut libero malesuada feugiat. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.\r\n', '&lt;p&gt;Nulla quis lorem ut libero malesuada feugiat. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Vivamus suscipit tortor eget felis porttitor volutpat. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur aliquet quam id dui posuere blandit. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.&lt;/p&gt;\r\n', 'Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.\r\n', '&lt;p&gt;Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.&lt;/p&gt;\r\n', 0, '2016-04-24 21:50:52', '0000-00-00 00:00:00', NULL),
(6, 4, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices ', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices ', 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.\r\n', '&lt;p&gt;Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.&lt;/p&gt;\r\n', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.\r\n', '&lt;p&gt;Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.&lt;/p&gt;\r\n', 1, '2016-04-24 21:54:49', '0000-00-00 00:00:00', NULL),
(7, 4, 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum a', 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum a', 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus.\r\n', '&lt;p&gt;Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus.&lt;/p&gt;\r\n', 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus.\r\n', '&lt;p&gt;Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Curabitur aliquet quam id dui posuere blandit. Proin eget tortor risus. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus.&lt;/p&gt;\r\n', 1, '2016-04-24 22:10:39', '0000-00-00 00:00:00', NULL),
(8, 4, 'secont slider ', 'bbbbbbbbbbbbbbbbbbbbbbbb', 'Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Pellentesque in ipsum id orci porta dapibus. Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim.\r\n', '&lt;p&gt;&lt;strong&gt;Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Pellentesque in ipsum id orci porta dapibus. Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim.&lt;/strong&gt;&lt;/p&gt;\r\n', 'Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Pellentesque in ipsum id orci porta dapibus. Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim.\r\n', '&lt;p&gt;&lt;strong&gt;Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Pellentesque in ipsum id orci porta dapibus. Cras ultricies ligula sed magna dictum porta. Cras ultricies ligula sed magna dictum porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim.&lt;/strong&gt;&lt;/p&gt;\r\n', 1, '2016-04-26 19:17:28', '0000-00-00 00:00:00', NULL),
(9, 4, 'Clean Code edit', 'bbbbbbbbbbbbbbbbb', 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n', '&lt;p&gt;Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&lt;/p&gt;\r\n', 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n', '&lt;p&gt;Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Cras ultricies ligula sed magna dictum porta. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.&lt;/p&gt;\r\n', 1, '2016-04-27 04:32:05', '0000-00-00 00:00:00', NULL),
(10, 4, 'Java', 'fsdfdfdf', 'vdgdfsdfsdfsdf\r\n', '&lt;p&gt;vdgdfsdfsdfsdf&lt;/p&gt;\r\n', 'dfsdfdsfsdfsffsdf\r\n', '&lt;p&gt;dfsdfdsfsdfsffsdf&lt;/p&gt;\r\n', 1, '2016-04-27 18:36:41', '0000-00-00 00:00:00', NULL),
(11, 4, 'dfdfdf', 'dffdfdf', 'dgdgdgsdgdfg\r\n', '&lt;p&gt;dgdgdgsdgdfg&lt;/p&gt;\r\n', 'vdfsdfdfsd\r\n', '&lt;p&gt;vdfsdfdfsd&lt;/p&gt;\r\n', 1, '2016-04-28 02:15:09', '0000-00-00 00:00:00', NULL),
(12, 4, 'Prodip check Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia ', 'dfgdfgdfgdfgdfgdfg', 'Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.\r\n', '&lt;p&gt;Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.&lt;/p&gt;\r\n', 'Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.\r\n', '&lt;p&gt;Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim.&lt;/p&gt;\r\n', 1, '2016-04-28 02:20:22', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles_categories_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_categories_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `category_id` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `articles_categories_mapping`
--

INSERT INTO `articles_categories_mapping` (`id`, `article_id`, `category_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(2, 2, '1,5', '2016-04-20 12:07:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, '2,4,5', '2016-04-20 19:34:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '4,5', '2016-04-24 21:48:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, '5,4,3', '2016-04-24 21:50:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '6,7', '2016-04-24 21:54:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, '1,3', '2016-04-24 22:10:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, '4', '2016-04-26 19:17:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, '8,7,6,5,4,3,2', '2016-04-27 04:32:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, '7,6', '2016-04-27 18:36:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 11, '8,7,6,5', '2016-04-28 02:15:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 12, '8,7,5', '2016-04-28 02:20:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `articles_images_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_images_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `articles_images_mapping`
--

INSERT INTO `articles_images_mapping` (`id`, `article_id`, `image_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(2, 2, 2, '2016-04-20 12:07:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 3, '2016-04-20 19:34:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 4, '2016-04-24 21:48:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 5, 5, '2016-04-24 21:50:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, 6, '2016-04-24 21:54:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 7, 7, '2016-04-24 22:10:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 8, 8, '2016-04-26 19:17:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 9, 9, '2016-04-27 04:32:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 10, 10, '2016-04-27 18:36:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 11, 11, '2016-04-28 02:15:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 12, 12, '2016-04-28 02:20:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `articles_menu_mapping`
--

CREATE TABLE IF NOT EXISTS `articles_menu_mapping` (
`id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `articles_menu_mapping`
--

INSERT INTO `articles_menu_mapping` (`id`, `article_id`, `menu_id`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, 11, 0, '2016-04-28 02:15:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 12, 0, '2016-04-28 02:20:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `parent_id` int(3) NOT NULL,
  `category_des` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`, `category_des`, `publication_status`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, 'News', 0, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'National', 0, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'International', 0, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Sports', 0, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Entertainment', 0, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Cricket', 4, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Football', 4, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Travelling', 5, '', 1, '2016-04-20 15:39:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(11) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `extention` varchar(100) NOT NULL,
  `size` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_name`, `extention`, `size`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, '1461131973megamind.jpg', 'jpg', '25294', '2016-04-20 11:59:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '14611324331017186_261013754039523_1524436942_n.jpg', 'jpg', '69641', '2016-04-20 12:07:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '146118134212745796_982515458509721_3386842757098237506_n.jpg', 'jpg', '118399', '2016-04-20 19:34:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '146151292010399513_1769903969904421_8424854827255820340_n.jpg', 'jpg', '12838', '2016-04-24 21:48:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '1461760961Animal cover.jpg', 'jpg', '463615', '2016-04-24 21:50:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '146151328911144790_142720342736421_2758783263070667905_n.jpg', 'jpg', '7382', '2016-04-24 21:54:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '14615142391chase_dt1.jpg', 'jpg', '74750', '2016-04-24 22:10:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '1461676647megamind.jpg', 'jpg', '25294', '2016-04-26 19:17:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '146178833212745796_982515458509721_3386842757098237506_n.jpg', 'jpg', '118399', '2016-04-27 04:32:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '146176060111036671_191603581177125_5385394535811278991_n.jpg', 'jpg', '52193', '2016-04-27 18:36:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '14617881091017186_261013754039523_1524436942_n.jpg', 'jpg', '69641', '2016-04-28 02:15:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '14617884221017186_261013754039523_1524436942_n.jpg', 'jpg', '69641', '2016-04-28 02:20:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
`id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sub_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `menu_des` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `sub_id`, `parent_id`, `url`, `menu_des`, `publication_status`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, 'Home', 0, 0, 'index.php', 'ffdfdfdfd\r\n', 1, '2016-04-27 19:03:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'National', 0, 0, 'category_archive.php?category_id=2', 'cdccd\r\n', 1, '2016-04-27 19:04:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'About Us', 0, 0, 'about_us.php', 'sdsfs\r\n', 1, '2016-04-27 19:07:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Contact Us', 0, 0, 'contact_us.php', 'dfdfd\r\n', 1, '2016-04-27 19:08:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `html_summary` text NOT NULL,
  `details` text NOT NULL,
  `html_details` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleled_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `user_id`, `title`, `sub_title`, `summary`, `html_summary`, `details`, `html_details`, `publication_status`, `created_at`, `modified_at`, `deleled_at`) VALUES
(1, 4, 'New', 'subtitle', 'summery\r\n', '&lt;p&gt;summery&lt;/p&gt;\r\n', 'this is new page\r\n', '&lt;p&gt;this is new page&lt;/p&gt;\r\n', 0, '2016-04-25 07:52:41', '0000-00-00 00:00:00', NULL),
(2, 4, 'New', 'subtitle', 'summery\r\n', '&lt;p&gt;summery&lt;/p&gt;\r\n', 'this is new page\r\n', '&lt;p&gt;this is new page&lt;/p&gt;\r\n', 0, '2016-04-25 07:54:13', '0000-00-00 00:00:00', NULL),
(3, 5, 'News', 'subtitles', 'summery\r\ns', '&lt;p&gt;summery&lt;/p&gt;\r\ns', 'this is new page\r\ns', '&lt;p&gt;this is new page&lt;/p&gt;\r\ns', 0, '2016-04-25 07:54:59', '0000-00-00 00:00:00', NULL),
(4, 4, 'Home', 'this is simple home page', 'simple content of this page\r\n', '&lt;p&gt;simple content of this page&lt;/p&gt;\r\n', 'simple content of this page\r\n', '&lt;p&gt;simple content of this page&lt;/p&gt;\r\n', 0, '2016-04-25 09:18:53', '0000-00-00 00:00:00', NULL),
(5, 4, 'New and', 'subtitle and', 'summery&nbsp;and\r\n', '&lt;p&gt;summery&amp;nbsp;and&lt;/p&gt;\r\n', 'this is new page&nbsp;and\r\n', '&lt;p&gt;this is new page&amp;nbsp;and&lt;/p&gt;\r\n', 0, '2016-04-25 09:43:50', '0000-00-00 00:00:00', NULL),
(6, 4, 'form', 'form', '&lt;form action=&quot;&quot; method=&quot;post&quot;&gt;\r\n&lt;input name=&quot;name&quot; value=&quot;Your Name&quot;&gt;\r\n&lt;input type=&quot;button&quot; value=&quot;Submit&quot;&gt;\r\n', '&lt;p&gt;&amp;lt;form action=&amp;quot;&amp;quot; method=&amp;quot;post&amp;quot;&amp;gt;&lt;br /&gt;\r\n&amp;lt;input name=&amp;quot;name&amp;quot; value=&amp;quot;Your Name&amp;quot;&amp;gt;&lt;br /&gt;\r\n&amp;lt;input type=&amp;quot;button&amp;quot; value=&amp;quot;Submit&amp;quot;&amp;gt;&lt;/p&gt;\r\n', '\r\n\r\n', '&lt;form action=&quot;&quot; method=&quot;post&quot;&gt;\r\n&lt;input name=&quot;name&quot; value=&quot;Your Name&quot;&gt;\r\n&lt;input type=&quot;button&quot; value=&quot;Submit&quot;&gt;', 0, '2016-04-25 09:46:45', '0000-00-00 00:00:00', NULL),
(7, 4, 'National', 'fsdfdfdf', 'dfdsfsdfsdfdsfd\r\n', '&lt;p&gt;dfdsfsdfsdfdsfd&lt;/p&gt;\r\n', 'dfdfsdfsdfdfsd\r\n', '&lt;p&gt;dfdfsdfsdfdfsd&lt;/p&gt;\r\n', 0, '2016-04-25 14:23:26', '0000-00-00 00:00:00', NULL),
(8, 4, 'Java', 'fsdfdfdf', 'dggdfgdfgd\r\n', '&lt;p&gt;dggdfgdfgd&lt;/p&gt;\r\n', 'sdgsdfsdgsdf\r\n', '&lt;p&gt;sdgsdfsdgsdf&lt;/p&gt;\r\n', 0, '2016-04-25 14:38:26', '0000-00-00 00:00:00', NULL),
(9, 4, 'Java', 'fsdfdfdf', 'dggdfgdfgd\r\n', '&lt;p&gt;dggdfgdfgd&lt;/p&gt;\r\n', 'sdgsdfsdgsdf\r\n', '&lt;p&gt;sdgsdfsdgsdf&lt;/p&gt;\r\n', 0, '2016-04-25 14:39:40', '0000-00-00 00:00:00', NULL),
(10, 4, 'dfdf', 'fdfdsfsd', 'dfsdfdfsdfsdfsd\r\n', '&lt;p&gt;dfsdfdfsdfsdfsd&lt;/p&gt;\r\n', 'dfsdfsdfsdfsdfs\r\n', '&lt;p&gt;dfsdfsdfsdfsdfs&lt;/p&gt;\r\n', 0, '2016-04-25 14:42:32', '0000-00-00 00:00:00', NULL),
(11, 4, 'About Us', 'about as', 'djfdsjfljsdlfjklsdjfljlsd\r\n', '&lt;p&gt;djfdsjfljsdlfjklsdjfljlsd&lt;/p&gt;\r\n', 'dfjsdkjfksdkfsdkjfksdj\r\n', '&lt;p&gt;dfjsdkjfksdkfsdkjfksdj&lt;/p&gt;\r\n', 0, '2016-04-26 10:52:34', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
`id` int(5) NOT NULL,
  `user_id` int(3) NOT NULL,
  `user_group_id` int(2) NOT NULL,
  `firstname` varchar(150) NOT NULL,
  `lastname` varchar(150) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `mobile_no` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `country` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `user_group_id`, `firstname`, `lastname`, `gender`, `mobile_no`, `address`, `country`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 0, 'Polash', 'Ahmed', 'Male', '19261212121', ' sasasasasa                                                                                                   ', 'Bangladesh', '1461698153megamind.jpg', '2016-04-27 01:15:53', '0000-00-00 00:00:00', NULL),
(2, 2, 0, 'Prodip', 'Roy', 'Male', '019264545454', 'asasasa                                                                                                    ', 'Bangladesh', '146169866112745796_982515458509721_3386842757098237506_n.jpg', '2016-04-27 01:24:21', '0000-00-00 00:00:00', NULL),
(3, 3, 0, 'Anich', 'Mia', 'Male', '19261212121', '   sddasdsdas                                                                                                 ', 'Bangladesh', '1461700101animal.jpg', '2016-04-27 01:48:21', '0000-00-00 00:00:00', NULL),
(4, 4, 0, 'Abeer ', 'Ahmed', 'Male', '01624545454', '                                                                                                                                                                qwqwqwq                                                                                                                                                                                                                                                ', 'Bangladesh', '146178670910399513_1769903969904421_8424854827255820340_n.jpg', '2016-04-27 03:01:48', '0000-00-00 00:00:00', NULL),
(6, 5, 0, 'Polash 4', 'Ahmed4', 'Female', '4419261212121', '      444                                                                                                333                                                   dfdfsdfsd                                                                                                                                                ', 'American Samoa', '1461709817Donald Duck.png', '2016-04-27 03:32:41', '0000-00-00 00:00:00', NULL),
(8, 6, 0, 'dfdfd', 'eeeeeee', 'Female', '01624545454', '              adsdsds                                                                                      ', 'Algeria', '1461735922emu.jpg', '2016-04-27 11:45:22', '0000-00-00 00:00:00', NULL),
(9, 7, 0, 'Satyajitbisv', 'Roy59efsdf', 'Male', '01791032835', '                                                                                     s                   13 no oradsgsg                                      55                                                                                                                                                                                                                                                                                  ', 'United Kingdom', '1461736448animal.jpg', '2016-04-27 11:49:11', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `title` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `reg_permission` tinyint(4) NOT NULL,
  `user_active` tinyint(4) NOT NULL,
  `footer` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `sqr_logo` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `wide_logo` varchar(222) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`title`, `subtitle`, `email`, `reg_permission`, `user_active`, `footer`, `sqr_logo`, `wide_logo`) VALUES
('Site titles', 'subtitles', 'alam@fosof.rogs', 0, 0, 'helllo this is fortteers', 'something.pngs', '');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
`id` int(3) NOT NULL,
  `title` varchar(255) NOT NULL,
  `caption` text NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_extention` varchar(150) NOT NULL,
  `slider_size` varchar(150) NOT NULL,
  `publication_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `caption`, `slider_image`, `slider_extention`, `slider_size`, `publication_status`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, 'aaaaaaaaaa', '&lt;h3&gt;Responsive Template.&lt;/h3&gt;\r\n\r\n&lt;p&gt;Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.&lt;/p&gt;\r\n', '146161117711051851_1382335335429694_1581800805037159797_n.jpg', '', '', 1, '2016-04-25 23:23:29', '0000-00-00 00:00:00', NULL),
(2, 'secont slider ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n', '146161115010952551_369368616576010_552449833867792577_n.jpg', 'jpg', '20506', 1, '2016-04-25 23:26:26', '0000-00-00 00:00:00', NULL),
(3, 'Third slider', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n', '146161120111077223_1384450088551552_7221894899960505263_n.jpg', 'jpg', '15859', 1, '2016-04-25 23:27:20', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(4) NOT NULL,
  `user_group_id` int(3) NOT NULL,
  `unique_id` varchar(100) NOT NULL,
  `username` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `unique_id`, `username`, `email`, `password`, `is_admin`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, '570ec0db50517', 'polash', 'prodip@gmail.com', '123456', 0, 0, '2016-04-14 03:57:47', '0000-00-00 00:00:00', NULL),
(2, 0, '570ec16c69d77', 'prodip', 'prodiproy@gmail.com', '123', 0, 1, '2016-04-14 04:00:12', '0000-00-00 00:00:00', NULL),
(3, 0, '570ec1c2d8c1e', 'anich', 'anich@gmail.com', '1234', 1, 1, '2016-04-14 04:01:38', '0000-00-00 00:00:00', NULL),
(4, 0, '570ec32e9faae', 'abeer', 'abeer@gmail.com', '12345678', 1, 1, '2016-04-14 04:07:42', '0000-00-00 00:00:00', NULL),
(5, 0, '571062d40341f', 'polash', 'prodip@gmail.com', '123456', 0, 1, '2016-04-15 09:41:08', '0000-00-00 00:00:00', NULL),
(6, 0, '57159ca3c9c74', 'pro', 'prodip@gmail.com', '1', 0, 1, '2016-04-19 08:49:07', '0000-00-00 00:00:00', NULL),
(7, 0, '57204c349243c', 'Satyajit', 'satyajit9000@gmail.com', '12345', 0, 1, '2016-04-27 11:20:52', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`title`), ADD KEY `title` (`title`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `articles_categories_mapping`
--
ALTER TABLE `articles_categories_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `articles_images_mapping`
--
ALTER TABLE `articles_images_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `articles_menu_mapping`
--
ALTER TABLE `articles_menu_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
