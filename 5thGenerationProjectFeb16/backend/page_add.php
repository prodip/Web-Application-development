<?php
include_once '../vendor/autoload.php';

?>

<?php include_once 'admin_header.php';?>

                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <h4 style="color: green; text-align: center;">
                            <?php
                            if (isset($_SESSION['page_save_mgs'])) {
                                echo $_SESSION['page_save_mgs'];
                                unset($_SESSION['page_save_mgs']);
                            }
                            ?>
                        </h4>
                        <div class="col-lg-12">
                            <form action="page_store.php" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-8 col-lg-12">
                                        <div class="main-box">
                                            <div class="clearfix">

                                                <h2>Add New Page</h2>

                                                <div class="form-group">
                                                    <label for="title"><h4>Title</h4></label>
                                                    <input type="text" name="title" class="form-control" id="title" placeholder="Enter A Title">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sub_title"><h4>Sub title</h4></label>
                                                    <input type="text" name="sub_title" class="form-control" id="title" placeholder="Enter A Sub Title">
                                                </div>


                                                <div class="form-group">
                                                    <label for="myeditor"><h4>Details</h4></label>
                                                    <textarea name="details" class="ckeditor" id="myeditor" rows="4" cols="50" required></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="myeditor"><h4>Summary</h4></label>
                                                    <textarea name="summary" class="ckeditor" id="myeditor" rows="4" cols="50" required></textarea>
                                                </div>




                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="graph-line" style="max-height: 290px;"></div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <input type="submit" class="btn btn-default submit" value="Save">
                                <input type="reset" class="btn btn-default submit"  value="Reset">
                            </form>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
   <?php include_once 'admin_footer.php';?>