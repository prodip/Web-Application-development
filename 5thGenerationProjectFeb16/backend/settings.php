<?php

include_once '../vendor/autoload.php';

use App\Settings;

$settingobj = new Settings();

$settings = $settingobj->show_settings();

if (!isset($settings['title']) || empty($settings['title'])) {
    $settingobj->insert();
}
//echo '<pre>';
//print_r($settings);
//exit();
?>

 <?php include_once 'admin_header.php';?>

        <div class="col-md-10" id="content-wrapper">
            <div class="row">
                <h4 style="color: green; text-align: center;">
                    <?php
                    if (isset($_SESSION['settings_save_mgs'])) {
                        echo $_SESSION['settings_save_mgs'];
                        unset($_SESSION['settings_save_mgs']);
                    }
                    ?>
                </h4>
                <div class="col-lg-12">

                    <div class="row">
                        <div class="col-md-10 col-lg-12">
                            <div class="main-box">
                                <form action="settings_stoer.php" method="POST" enctype="multipart/form-data">
                                    <div class="clearfix">
                                        <h2>Setting</h2>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="title"><h4>Site Title</h4></label>
                                                <input type="text" name="title" class="form-control" id="title"
                                                       placeholder="Enter A Title"
                                                       value="<?php echo $settings['title'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="subtitle"><h4>Site Subtile</h4></label>
                                                <input type="text" name="subtitle" class="form-control" id="subtitle"
                                                       placeholder="Enter A Title"
                                                       value="<?php echo $settings['subtitle'] ?>">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="system_email"><h4>System Email</h4></label>
                                                <input type="text" name="system_emil" class="form-control" id="system_email"
                                                       placeholder="Enter A Title"
                                                       value="<?php echo $settings['email'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="footer"><h4>Footer Text</h4></label>
                                                <input type="text" name="footer" class="form-control" id="footer"
                                                       placeholder="Enter A Title"
                                                       value="<?php echo $settings['footer'] ?>">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="reg_permission"><h4>Registration Permission</h4></label><br/>
                                                <input type="radio" name="reg_permission"
                                                       value="1"<?php if ($settings['reg_permission'] == 1) {
                                                    echo "checked";
                                                } ?>>Allow user registration
                                                <input type="radio" name="reg_permission"
                                                       value="0"<?php if ($settings['reg_permission'] == 0) {
                                                    echo "checked";
                                                } ?>>Registration not allow
                                            </div>
                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="user_active"><h4>After Signup</h4></label><br/>
                                                <input type="radio" name="activation"
                                                       value="1"<?php if ($settings['user_active'] == 1) {
                                                    echo "checked";
                                                } ?>>Auto approve
                                                <input type="radio" name="activation"
                                                       value="0"<?php if ($settings['user_active'] == 0) {
                                                    echo "checked";
                                                } ?>>Manual approve
                                                <input type="radio" name="activation"
                                                       value="2"<?php if ($settings['user_active'] == 2) {
                                                    echo "checked";
                                                } ?> disabled="disabled">Email varification
                                            </div>
                                        </div>

                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="square_logo"><h4>Site Logo Square</h4></label><?php if (isset($settings['sqr_logo']) && !empty($settings['sqr_logo'])) { ?>
                                                    <img src="<?php echo "../img/logos/" . $settings['sqr_logo']; ?>" width="100" height="100">
                                                <?php } ?>
                                                <input type="file" name="square_logo" class="form-control" id="square_logo"
                                                       placeholder="Enter A Title"
                                                       value="<?php echo $settings['title'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="wide_logo"><h4>Site Logo Wide</h4></label>
                                                <?php if (isset($settings['wide_logo']) && !empty($settings['wide_logo'])) { ?>
                                                    <img src="<?php echo "../img/logos/" . $settings['wide_logo']; ?>" width="100" height="100">
                                                <?php } ?>
                                                <input type="file" name="wide_logo" class="form-control" id="wide_logo"
                                                       placeholder="Enter A Title"
                                                       value="<?php echo $settings['subtitle'] ?>">
                                            </div>
                                        </div>

                                        <input type="hidden" name="oldimage1"
                                               value="<?php echo $settings['sqr_logo'] ?>">
                                        <input type="hidden" name="oldimage2"
                                               value="<?php echo $settings['wide_logo'] ?>">

                                        <input type="submit" class="btn btn-default submit" value="Save">
                                        <input type="reset" class="btn btn-default submit" value="Reset"></div>
                                </form>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="graph-line" style="max-height: 290px;"></div>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>


            </div>
        </div>
    </div>
</div>
</div>
<?php include_once 'admin_footer.php';?>