<?php
if (!isset($_SESSION)) {
    session_start();
}
if ($_SESSION['user_id']) {
    
} else {
    header('location:../index.php');
}
?>
<?php
include_once '../vendor/autoload.php';

use App\Users;
use App\Profiles;
use App\Pages;

$pageobj = new Pages();
$profileobj = new Profiles();
$userobj = new Users();
//$all_users = $userobj->view_all_users();

$user_all_info = $profileobj->view_all_users();

$page_info = $pageobj->manage_page();
//echo '<pre>';
//print_r($page_info);
//exit();
?>
<?php include_once 'admin_header.php';?>

                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="clearfix">
                                <h2 class="pull-left">Manage Pages</h2>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="table-responsive">
                                            <table class="table user-list">
                                                <thead>
                                                    <tr>
                                                        <th><span>Posted By</span></th>
                                                        <th><span>Page Title</span></th>
                                                        <th class="text-center"><span>Publication Status</span></th>
                                                        <th><span>Posted Date</span></th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    foreach ($page_info as $v_page_info) {
                                                        ?>
                                                        <tr>
                                                            <td>
<!--                                                                --><?php //if (isset($v_page_info['image']) && !empty($v_page_info['image'])) { ?>
<!--                                                                    <img src="../img/profile_pics/--><?php //echo $v_page_info['image']; ?><!--" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>-->
<!--                                                                --><?php //} else { ?>
<!--                                                                    <img src="../img/samples/Unknown_Person.png" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>-->
<!---->
<!--                                                                --><?php //} ?>
<!--                                                                <a href="user_view.php?id=--><?php //echo $v_page_info['page_id']; ?><!--" class="user-link">--><?php //echo $v_page_info['firstname'] . " " . $v_page_info['lastname']; ?><!--</a>-->
<!--                                                                <span class="user-subhead">-->
<!--                                                                    --><?php
//                                                                    if ($v_page_info['is_admin'] == 1) {
//                                                                        echo "Admin";
//                                                                    } else {
//                                                                        echo "User";
//                                                                    }
//                                                                    ?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <?php echo $v_page_info['title']; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php if ($v_page_info['publication_status'] == 1) { ?>
                                                                <a href = "page_unpublished.php?id=<?php echo $v_page_info['id']; ?>">
                                                                        <span class = "label label-danger">Unpublised</span>
                                                                    </a>
                                                                <?php } else { ?>
                                                                    <a href = "page_published.php?id=<?php echo $v_page_info['id']; ?>">
                                                                        <span class = "label label-success">Published</span>
                                                                    </a>
                                                                <?php } ?>

                                                            </td>
                                                            <td style="width: 10%;">
                                                                <?php
                                                                $data = $v_page_info['created_at'];
                                                                $date_format = date('d M Y H:i A', strtotime($data));
                                                                echo $date_format;
                                                                ?>
                                                            </td>
                                                            <td style="width: 15%;">
                                                                <a href="user_view.php?id=<?php echo $v_page_info['id']; ?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="page_edit.php?id=<?php echo $v_page_info['id']; ?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="page_delete.php?id=<?php echo $v_page_info['id']; ?>" class="table-link danger">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
      <?php include_once 'admin_footer.php';?>