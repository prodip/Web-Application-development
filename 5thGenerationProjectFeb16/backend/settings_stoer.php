<?php
include_once '../vendor/autoload.php';

use App\Settings;
$settingobj = new Settings();


$image_name1 = $_FILES['square_logo']['name'];
$image_type1 = $_FILES['square_logo']['type'];
$image_location1 = $_FILES['square_logo']['tmp_name'];
$image_size1 = $_FILES['square_logo']['size'];

$image_name2 = $_FILES['wide_logo']['name'];
$image_type2 = $_FILES['wide_logo']['type'];
$image_location2 = $_FILES['wide_logo']['tmp_name'];
$image_size2 = $_FILES['wide_logo']['size'];


$m1 = explode('.', $image_name1);
$file_ext1 = strtolower(end($m1));
$image1 = "sqr_logo.".$file_ext1;

$m2 = explode('.', $image_name2);
$file_ext2 = strtolower(end($m2));
$image2 = "wide_logo.".$file_ext2;


$image_type_support = array('jpg', 'jpeg', 'png', 'gif');

if(in_array($file_ext1, $image_type_support) === false){
    echo 'Invalid Format!';
    header('lcoation:settings.php');
}else{
    if(isset($_POST['oldimage1']) && !empty($_POST['oldimage1'])){
        unlink("../img/logos/".$_POST['oldimage1']);
    }
    $_POST['sqr_logo'] = $image1;
    move_uploaded_file($image_location1, '../img/logos/'.$image1);
}
if(in_array($file_ext2, $image_type_support) === false){
    echo 'Invalid Format!';
    header('lcoation:settings.php');
}else{
    if(isset($_POST['oldimage2']) && !empty($_POST['oldimage2'])){
        unlink("../img/logos/".$_POST['oldimage2']);
    }
    $_POST['wide_logo'] = $image2;
    move_uploaded_file($image_location2, '../img/logos/'.$image2);
}

$settingobj->prepare($_POST);
$settingobj->update($_POST);
?>