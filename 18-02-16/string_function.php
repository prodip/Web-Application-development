<?php

$str = "His father's name is X";
print_r(addslashes($str));

echo '<br>';
$str_exp = "Hi, I am Prodip!";
echo '<pre>';
$arr = explode(' ' ,$str_exp);
print_r($arr);

echo '<br>';
print_r(implode(' ', $arr));

echo '<br>';
$str_htmlentities = "All <b>copyright</b> reserve © to me.";
echo htmlentities($str_htmlentities);

echo '<br>';
$str_html = "All <b>copyright</b> reserve © to me.";
$s = htmlentities($str_html);
echo htmlentities($s);

echo '<br>';
$sr_ltrim = '/t @©I am Prodip!/t.';
print_r(ltrim($sr_ltrim, "/t@© "));
echo '<br>';
$sr_rtrim = '/t @©I am Prodip!/t.';
print_r(rtrim($sr_rtrim, "/t."));
echo '<br>';
$sr_trim = '/t @©I am Prodip!/t.';
print_r(trim($sr_trim, "/t."));

echo '<br>';
echo nl2br(" I am Prodip.\n What's yours?");

echo '<br>';
$str_p = 'Prodip';
print_r(str_pad($str_p, 20, '+', STR_PAD_LEFT));
echo '<br>';
print_r(str_pad($str_p, 20, '+', STR_PAD_RIGHT));
echo '<br>';
print_r(str_pad($str_p, 20, '+', STR_PAD_BOTH));

echo '<br>';
$str_rep = 'Prodip <br>';
print_r(str_repeat($str_rep, 10));

echo '<br>';
$srt_replace = " Hi, My name is Anich!";
print_r(str_replace('Anich', 'Prodip', $srt_replace));
echo '<br>';
print_r(str_replace(' ', '', $srt_replace));

echo '<br>';
print_r(str_split("Hi, I am Prodip" , 2));

echo '<br>';
$str_tags = "<b>Prodip</b>.<p> Where are <h1>You</h1>?</p>";
print_r(strip_tags($str_tags, '<b><p><h1>'));


